"""
Library of functions to parse and retrieve items from the function library.

Copyright 2020 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2020 by Alexander Mueller.
"""

import json

from SDTL import *

# load the function library JSON into a dictionary
with open("SDTL_Function_Library_04.json", "r") as library_file:
    library = json.loads(library_file.read())

# state is one of [horizontal, vertical, collapse, logical]
SDTL_map = {}   # state -> (python function name -> SDTL function name)
offset_map = {} # state -> (python function name -> list of offset values)
req_map = {}    # state -> (python function name -> (SDTL parameter position -> boolean isRequired))
def_map = {}    # state -> (python function name -> (SDTL parameter position -> default value))
name_map = {}   # state -> (python function name -> (python parameter name -> SDTL parameter position))
pos_map = {}    # state -> (python function name -> (python parameter position -> SDTL parameter position))
#                                                         zero-indexed integer -> one-indexed string

# populate the maps declared above (global logic because this only needs to happen once)
for state, functions in library.items():
    SDTL_map[state] = {}
    offset_map[state] = {}
    def_map[state] = {}
    req_map[state] = {}
    name_map[state] = {}
    pos_map[state] = {}
    for function in functions:
        for variant in function["Python"]:
            function_name = variant["function_name"]
            SDTL_map[state][function_name] = function["SDTLname"]
            offset_map[state][function_name] = []
            req_map[state][function_name] = {}
            def_map[state][function_name] = {}
            name_map[state][function_name] = {}
            pos_map[state][function_name] = {}
            for index, parameter in enumerate(variant["parameters"]):
                if parameter["param"] is None:
                    continue
                # This has to be explicit because isRequired might be null
                if parameter["isRequired"]:
                    req_map[state][function_name][parameter["position"]] = True
                else:
                    req_map[state][function_name][parameter["position"]] = False
                if parameter["defaultValue"]:
                    def_map[state][function_name][parameter["position"]] = parameter["defaultValue"]
                if parameter["index_offset"]:
                    offset_map[state][function_name].append(parameter["index_offset"])
                else:
                    offset_map[state][function_name].append(0)
                if parameter["position"]:
                    pos_map[state][function_name][index] = int(float(parameter["position"]))
                if parameter["name"] and len(parameter["name"]) > 0:
                    try:
                        name_map[state][function_name][parameter["name"]] = int(float(parameter["position"]))
                    except TypeError:
                        name_map[state][function_name][parameter["name"]] = parameter["position"]

def makeValue(value):
    if isinstance(value, str):
        # TODO: remove this insanity when the function library is fixed
        try:
            return NumericConstantExpression(int(value))
        except ValueError:
            return StringConstantExpression(value)
    elif isinstance(value, bool):
        return BooleanConstantExpression(value)
    else:
        return NumericConstantExpression(value)

# for a given python function, return the SDTL equivalent
def get_SDTL_function(function_name, positional, keyword, state, named_agg=False):
    arguments = []
    arguments_dict = {}

    # get the SDTL position for each positional argument
    for index, argument in enumerate(positional):
        if isinstance(argument, AllNumericVariablesExpression):
            arguments_dict["1"] = argument
        else:
            #print(pos_map[state][function_name])
            #print(pos_map[state][function_name][index])
            arguments_dict[int(pos_map[state][function_name][index])] = argument

    # get the SDTL position for each keyword argument
    for name, argument in keyword.items():
        if name != "axis":
            arguments_dict[int(name_map[state][function_name][name])] = argument

    # reshape the dictionary back into a list
    for position, value in sorted(arguments_dict.items()):
        arguments.append(FunctionArgument(int(position), value))
        """
        if state == "collapse" and not named_agg:
            arguments.append(FunctionArgument(int(position), value))
        else:
            arguments.append(FunctionArgument(int(position) + 1, value))
        """

    # apply offsets to fix off-by-one errors in the python values
    for index, argument in enumerate(arguments):
        if isinstance(argument.argumentValue, NumericConstantExpression):
            offset = int(offset_map[state][function_name][index])
            value = int(argument.argumentValue.value)
            arguments[index].argumentValue.value = str(value + offset)

    # if any required arguments are missing, use their default value
    for position, required in req_map[state][function_name].items():
        if required:
            try:
                arguments[int(position - 1)]
            except IndexError:
                # TODO: identify and remove null default values in the function library
                # so that this dangerous workaround can be avoided altogether
                try:
                    default = def_map[state][function_name][position]
                    #arguments.append(FunctionArgument(int(position) + 1, makeValue(default)))
                    arguments.append(FunctionArgument(int(position), makeValue(default)))
                except KeyError:
                    pass

    # return a FunctionCallExpression
    return FunctionCallExpression(name=SDTL_map[state][function_name], arguments=arguments)
