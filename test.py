"""
Testing framework for Python to SDTL conversion.

Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2019 by Alexander Mueller.
"""

import visitor
import SDTL

import unittest
import time
import subprocess
import requests
import pathlib
import json
import glob
import ast
import astpretty

# Read and return the correct JSON output for this test case
def readJSON(filepath):
    with open(filepath, "r") as file:
        json_dict = json.loads(file.read())
    return json_dict

# Parse the python code and return the JSON output the parser produced
def readPython(filepath, file_variables={}, dataframe_variables={}):
    makeProductionTest(filepath, file_variables)

    commands = visitor.convert_file(filepath, file_variables=file_variables, dataframe_variables=dataframe_variables).commands

    writeTestResults(filepath, commands)
    return SDTL.clean_object(commands)

# Read the input JSON, send it to the parser, and report the output
def readInput(filepath):
    with open(filepath, "r") as file:
        time.sleep(1)
        r = requests.post("http://localhost:8095/api/python-to-sdtl", data=file.read())
    return json.loads(r.text)["commands"]

# Write the parser output to a file for easier JSON inspection if the test fails
def writeTestResults(filepath, commands):
    output_path = "./test/output_json/"
    with open(output_path + filepath.split("/")[-1][:-3] + ".json", "w") as file:
        file.write(SDTL.get_json(commands))

# Make the JSON input that the production code will expect from this test case
def makeProductionTest(filepath, file_variables={}):
    prod_path = "./test/input_json/"
    parameters = { "data_file_descriptions": [] }
    for file, variables in file_variables.items():
        description = {
            "input_file_name": file,
            "DDI_XML_file": None,
            "file_name_DDI": None,
            "variables": variables
        }
        parameters["data_file_descriptions"].append(description)
    with open(filepath, "r") as source:
        parameters["python"] = source.read()
    output = { "parameters": parameters }
    with open(prod_path + filepath.split("/")[-1][:-3] + ".json", "w") as file:
        file.write(json.dumps(output, indent=4))

class TestCommand(unittest.TestCase):

    # regenerate ASTs of the test files in case they've been updated
    # (these ASTs are only relevant for manual inspection of why a test failed)
    @classmethod
    def setUpClass(cls):
        for test in glob.glob("./test/python/*.py"):
            with open(test, "r") as test_file:
                test_ast = astpretty.pformat(ast.parse(test_file.read()), show_offsets=False)
            with open("./test/ast/" + pathlib.Path(test).stem + ".txt", "w") as ast_file:
                ast_file.write(test_ast)

    #maxDiff = None

    def test_append(self):
        self.assertEqual(readPython("./test/python/append.py", file_variables={ "top.csv": ["A", "B"], "bottom.csv": ["A", "B", "C"]}),
                         readJSON("./test/sdtl/append.json"))

    def test_cases(self):
        self.assertEqual(readPython("./test/python/cases.py", file_variables={ "df.csv": ["A", "B"] }),
                         readJSON("./test/sdtl/cases.json"))

    def test_collapse(self):
        self.assertEqual(readPython("./test/python/collapse.py", file_variables={ "scores.csv": ["Age", "Gender", "Score"] }),
                         readJSON("./test/sdtl/collapse.json"))

    def test_comment(self):
        self.assertEqual(readPython("./test/python/comment.py"), 
                         readJSON("./test/sdtl/comment.json"))

    def test_compute(self):
        self.assertEqual(readPython("./test/python/compute.py", file_variables={ "df.csv": ["A", "B"], "temps.csv": ["fahrenheit"]}),
                         readJSON("./test/sdtl/compute.json"))
        self.assertEqual(readPython("./test/python/compute2a.py", file_variables={ "temps.csv": ["fahrenheit"]}),
                         readJSON("./test/sdtl/compute2a.json"))
        self.assertEqual(readPython("./test/python/compute2b.py", file_variables={ "temps.csv": ["fahrenheit"]}),
                         readJSON("./test/sdtl/compute2b.json"))

    def test_cut(self):
        self.assertEqual(readPython("./test/python/cut.py", file_variables={ "GSS_sample.csv": [ "ADULTS", "AGE", "BIGBAND", "BLUES", "BLUGRASS", "CASEID", "CHILDS", "CLASSICL", "COHORT", "CONROCK", "COUNTRY", "DEGREE", "EARNRS", "EDUC", "FOLK", "FORMWT", "GENDER1", "GENDER2", "GENDER3", "GENDER4", "GENDER5", "GENDER6", "GENDER7", "GENDER8", "GENDER9", "GOSPEL", "HHRACE", "HHTYPE", "HRS1", "HVYMETAL", "ID", "INCOME", "ISSP", "JAZZ", "LATIN", "MARCOHRT", "MARITAL", "MOODEASY", "MUSICALS", "NATSPAC", "NEWAGE", "OLD1", "OLD2", "OLD3", "OLD4", "OLD5", "OLD6", "OLD7", "OLD8", "OLD9", "OLDIES", "OPERA", "OVERSAMP", "PARTYID", "RACE", "RAP", "REALINC", "REALRINC", "REGGAE", "REGION", "RESPNUM", "RINCOME", "RPLACE", "SAMPCODE", "SAMPLE", "SEX", "SIBS", "SIZE", "UNRELAT", "WRKSTAT", "YEAR" ] }), 
                         readJSON("./test/sdtl/cut.json"))
        self.assertEqual(readPython("./test/python/cut2.py", file_variables={ "cut_test.csv": [ "varX"] }), 
                         readJSON("./test/sdtl/cut2.json"))

    def test_drop(self):
        self.assertEqual(readPython("./test/python/drop.py", file_variables={"drop.csv": ["A", "B", "C"]}),
                         readJSON("./test/sdtl/drop.json"))

    def test_keep(self):
        self.assertEqual(readPython("./test/python/keep.py", file_variables={"keep.csv": ["A", "B", "C"]}),
                         readJSON("./test/sdtl/keep.json"))

    def test_load(self):
        self.assertEqual(readPython("./test/python/load.py", file_variables={ "data.csv": ["a", "b"], "data.xlsx": ["a", "b"], "data.sas7bdat": ["a", "b"], "data.dta": ["a", "b"]}), 
                         readJSON("./test/sdtl/load.json"))

    def test_merge(self):
        self.assertEqual(readPython("./test/python/merge.py", file_variables={ "left.csv": ["caseID", "firstName", "income", "famID"], "right.csv": ["rowID", "lastName", "income", "famID2"] }),
                         readJSON("./test/sdtl/merge.json"))

    def test_rename(self):
        self.assertEqual(readPython("./test/python/rename.py", file_variables={ "rename.csv": ["A", "B"]}), 
                         readJSON("./test/sdtl/rename.json"))

    def test_save(self):
        self.assertEqual(readPython("./test/python/save.py", file_variables={"df.csv": ["a", "b"]}), 
                         readJSON("./test/sdtl/save.json"))

    def test_sort(self):
        self.assertEqual(readPython("./test/python/sort.py", file_variables={ "df.csv": ["A", "B", "C"] }),
                         readJSON("./test/sdtl/sort.json"))

    def test_strings(self):
        self.assertEqual(readPython("./test/python/strings.py", file_variables={ "stringtest.csv": ["first", "second"] }),
                         readJSON("./test/sdtl/strings.json"))

class TestAPI(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        subprocess.Popen("python main.py &", shell=True)

    def test_all(self):
        for test_path in glob.glob("./test/input_json/*.json"):
            if "cases" in test_path or "collapse" in test_path:
                continue
            self.assertEqual(readInput(test_path), 
                             readJSON(test_path.replace("input_json", "sdtl")))

    @classmethod
    def tearDownClass(cls):
        subprocess.check_output("kill $(lsof -t -i :8095)", shell=True)

class TestDocker(unittest.TestCase):

    #@classmethod
    #def setUpClass(cls):
    #    subprocess.call(["docker", "build", "-t", "registry.gitlab.com/c2metadata/python-to-sdtl", "."])
    #    subprocess.call(["docker", "run", "-d", "-p", "8095:8095", "registry.gitlab.com/c2metadata/python-to-sdtl"])

    def test_all(self):
        for test_path in glob.glob("./test/input_json/*.json"):
            if "strings" in test_path:
                continue
            self.assertEqual(readInput(test_path), 
                             readJSON(test_path.replace("input_json", "sdtl")))

    #@classmethod
    #def tearDownClass(cls):
    #    subprocess.check_output("docker rm -f $(docker ps -a -q)", shell=True)

unittest.main()