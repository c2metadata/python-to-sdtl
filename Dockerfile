FROM archlinux

EXPOSE 8095

COPY . /usr/src/app

WORKDIR /usr/src/app

RUN echo "nameserver 8.8.8.8\n" >> /etc/resolv.conf

RUN yes | pacman -Syu 

RUN yes | pacman -S python python-pip

RUN pip install flask waitress astpretty requests

CMD ["python", "main.py"]
