"""
API Controller for the parser endpoint.

Copyright 2020 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2020 by Alexander Mueller.
"""

import ast
import astpretty
import flask
import json
import requests
import waitress

import visitor
import SDTL

app = flask.Flask(__name__)

# Hello World endpoint to verify that the parser is up and running
@app.route("/api/test", methods=["GET", "POST"])
def hello():

    # if this is a GET request, just return "Hello World"
    if flask.request.method == "GET":
        return "Hello World"

    # if this is a POST request, return what was posted
    if flask.request.method == "POST":
        input_json = flask.request.get_json(force=True)
        return json.dumps(input_json, indent=4)

# Returns the raw abstract syntax tree of the given python file
@app.route("/api/ast", methods=["POST"])
def getast():

    if flask.request.method == "POST":

        python_file = flask.request.files["python"].read().decode()

        test_ast = astpretty.pformat(ast.parse(python_file), show_offsets=False)

        return test_ast

# Endpoint to create a JSON test case from a python file
@app.route("/api/make-test", methods=["POST"])
def maketest():

    if flask.request.method == "POST":

        test_case = { "parameters": { "data_file_descriptions": [] } }
        var_url = "http://c2metadata.mtna.us/api/updater/api/info/variables/names"

        #input_json = flask.request.get_json(force=True)
        #input_json = flask.request.form

        ddi_xml_file = flask.request.files["xml"]
        ddi_filename = ddi_xml_file.filename

        variables = requests.post(url=var_url, files={"file": ddi_xml_file})

        description = {
            "input_file_name": flask.request.form["csv"],
            "DDI_XML_file": ddi_filename,
            "file_name_DDI": None,
            "variables": variables.json()
        }
        test_case["parameters"]["data_file_descriptions"].append(description)

        # assuming we get a bunch of filename -> variables as input
        """for key, value in input_json.items():
            description = {
                "input_file_name": key,
                "DDI_XML_file": None,
                "file_name_DDI": None,
                "variables": value
            }
            test_case["parameters"]["data_file_descriptions"].append(description)"""

        test_case["parameters"]["python"] = flask.request.files["python"].read().decode()

        return json.dumps(test_case, indent=4)

# Send the POST request to this URL
@app.route("/api/python-to-sdtl", methods=["POST"])
def parse():

    # Make sure that this is not a GET request
    if flask.request.method == "POST":

        # The data was sent as JSON, so we need to decode it as JSON
        parameters = flask.request.get_json(force=True)["parameters"]

        # The converter expects a string or file-like for python parsing
        python_file = parameters["python"]

        # Populate the dictionary of existing variables in each file
        descriptions = parameters["data_file_descriptions"]

        # Remove extension from input_file_name
        for I in range(len(descriptions)):
                descriptions[I]["input_file_name"] = descriptions[I]["input_file_name"].split(".")[0]

        variables = {
            description["input_file_name"]: description["variables"]
            for description in descriptions
        }

        # Pass everything to the parser to get the SDTL string
        commands = visitor.convert_string(python_file, file_variables=variables)
        return json.dumps(SDTL.clean_object(commands))


# development server
#app.run(host="127.0.0.1", port=8080, debug=False)

# production server
waitress.serve(app, host="0.0.0.0", port=8095)
