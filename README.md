# python-to-sdtl

The [Pandas](https://pandas.pydata.org/) library is an API for statistical transformation written in the Python programming language. The goal of this project is to take Pandas code as input and output the equivalent SDTL.

First, the Python code is read in as a JSON parameter via the REST API described below. It is then parsed into an abstract syntax tree using the `ast` library, from which visitor functions process the tree and output the resulting SDTL. This SDTL is returned as a standalone JSON document which is then posted to the REST API.

# Using the Docker Image

Pull and run the latest image from the registry at https://gitlab.com/c2metadata/python-to-sdtl/container_registry
```
docker pull registry.gitlab.com/c2metadata/python-to-sdtl:latest

docker run -d -p 8095:8095 registry.gitlab.com/c2metadata/python-to-sdtl
```

# API Endpoints

Python: `/api/python-to-sdtl`
```
curl -X POST "http://localhost:8095/api/python-to-sdtl" -d @compute.json
```
There are currently fifteen examples of mock input in the `/test/input_json` directory, in which the `compute.json` referenced above resides. The parser output for each of these tests (with the known exceptions of `cases.json` and `collapse.json`) should match the corresponding file in the `/test/stdl` directory.

Status: `/api/test`

The following should return `Hello World`:
```
curl "http://localhost:8095/api/test"
```
To test that the input JSON is being read correctly, use the following:
```
curl -X POST "http://localhost:8095/api/test" -d "{\"input\": \"test\"}"
```
Provided that the data given is valid JSON (and properly escaped), this should return a pretty printed version of the input.

# Working Command Examples

The following examples of commands which parse correctly under the current version are taken from the test cases in the `/test/python` directory:

**AppendDatasets**

| Python Code | Description |
|-------------|-------------|
| <pre>`appended = top.append(bottom)`</pre> | Standard method of appending two dataframes |
| <pre>`concatenated = pd.concat([top, bottom])`</pre> | Alternate method |
| <pre>`concat_inner = pd.concat([top, bottom], join="inner")`</pre> | This rejects any variables not in common between the two dataframes |

**Collapse**

<table>
    <thead>
        <tr>
            <th>Python Code</th>
            <th>Description</th>
        <tr>
    </thead>
    <tbody>
        <tr>
            <td><pre>median_age = scores.groupby("Age").median()</pre></td>
            <td>One group by variable and one collapse function</td>
        </tr>
        <tr>
            <td><pre>group_sum = scores.groupby(["Gender", "Age"]).sum()</pre></td>
            <td>Multiple group by variables</td>
        </tr>
        <tr>
            <td><pre>non_index = scores.groupby("Gender", as_index=False).mad()</pre></td>
            <td>Include the group by variable as a proper variable instaed of an index</td>
        </tr>
        <tr>
            <td><pre>single_agg = scores.groupby("Gender").agg("var")</pre></td>
            <td>Alternate way of adding a collapse function</td>
        </tr>
        <tr>
            <td><pre>multiple_agg = scores.groupby("Gender").agg(["quantile", "nunique", "skew"])</pre></td>
            <td>Multiple collapse functions (note that this results in one output variable per combination of group by variable and analysis variable)</td>
        </tr>
        <tr>
            <td>
<pre>named_agg = scores.groupby("Gender").agg(
    youngest=pd.NamedAgg(column="Age", aggfunc="min"),
    oldest=pd.NamedAgg(column="Age", aggfunc="max"),
    mean_score=pd.NamedAgg(column="Score", aggfunc="mean")
)</pre>
            </td>
            <td>Multiple collapse functions with renamed output variables</td>
        </tr>
    </tbody>
</table>

**Comment**

```python
"""This is a single-line docstring"""

#This is a single-line comment

"""
This
is
a
multi-
line
docstring
"""

"docstring with one set of quotes"

'docstring with one set of single-quotes'

'''docstring with three single-quotes'''

'''multi-
l
i
n
e
docstring with
three single quotes'''
```

**Compute**

<table>
    <thead>
        <tr>
            <th>Python Code</th>
            <th>Description</th>
        <tr>
    </thead>
    <tbody>
        <tr>
            <td><pre>df["A"] = 3</pre></td>
            <td>Compute a new variable and set all of its values to a scalar</td>
        </tr>
        <tr>
            <td><pre>df["B"] = df["B"] + 6.5</pre></td>
            <td>Compute a new value for a single variable</td>
        </tr>
        <tr>
            <td><pre>df["C"] = df["A"] - df["B"]</pre></td>
            <td>Compute a new variable from existing variables</td>
        </tr>
        <tr>
            <td><pre>temps_c = temps.assign(celsius=((temps["fahrenheit"] - 32) * 5 / 9))</pre></td>
            <td>Alternate method of computing a variable</td>
        </tr>
        <tr>
            <td><pre>temps_c = temps.assign(celsius=lambda x: (x.fahrenheit - 32) * 5 / 9)</pre></td>
            <td>Different syntax for the same operation</td>
        </tr>
        <tr>
            <td>
<pre>temps_k = temps.assign(celsius=lambda x: ((x["fahrenheit"] - 32) * 5 / 9), 
                       kelvin=lambda x : (x["celsius"] + 273))</pre>
            </td>
            <td>Compute two variables at once, with the second dependent on the first</td>
        </tr>
    </tbody>
</table>

**DropCases**

| Python Code | Description |
|-------------|-------------|
| <pre>`case5 = df.dropna(how="all")`</pre> | Drop cases where the value of *all* variables is missing |

**DropVariables**

| Python Code | Description |
|-------------|-------------|
| <pre>`drop = df.drop(columns="A")`</pre> | Drop a single variable |
| <pre>`drop_axis = df.drop(["A"], axis=1)`</pre> | Alternate syntax for dropping a single variable |
| <pre>`drop_sugar = df.drop("A", axis="columns")`</pre> | More syntacic sugar for the same operation |
| <pre>`df.drop(columns=["B", "C"], inplace=True)`</pre> | Drop multiple variables in-place (i.e. operate on the current dataframe rather than outputting to a new one) |

**KeepCases**

| Python Code | Description |
|-------------|-------------|
| <pre>`case = df[df["A"] > 2]`</pre> | Keep cases (rows) where the value of `A` is greater than 2 |
| <pre>`case2 = df[(df["A"] < 4) & (~(df["B"] == 3))]`</pre> | Keep cases where `A` is greater than 4 and `B` is not equal to 3 |
| <pre>`case3 = df[df.isin([2, 3]).all(axis=1)]`</pre> | Keep cases where the values of *all* variables are present in the list `[2,3]` |
| <pre>`case4 = df[df.isna().any(axis=1)]`</pre> | Keep cases where the value of *any* variable is missing |

**KeepVariables**

| Python Code | Description |
|-------------|-------------|
| <pre>`kept = df[["A", "B"]]`</pre> | Keep the specified variables in the output dataframe (to do this operation in place, name the output dataframe the same as the input) |

**Load**

| Python Code | Description |
|-------------|-------------|
| <pre>`csv_df = pd.read_csv("data.csv")`</pre> | Load a dataframe from a CSV file |
| <pre>`xls_df = pd.read_excel('data.xlsx')`</pre> | Load from an Excel file (also demonstrates use of single quotes) |
| <pre>`sas_df = pd.read_sas("""data.sas7bdat""")`</pre> | Load from a SAS file (triple quotes in this context are interpreted as a string, not a docstring) |
| <pre>`stata_df = pd.read_stata('''data.dta''')`</pre> | Load from a Stata file (also demonstrates string context of triple single quotes) |

**MergeDatasets**

For the merge examples, it may help to know the layout of these dataframes:
- `left` has variables `caseID`, `firstName`, `income`, `famID`
- `right` has variables `rowID`, `lastName`, `income`, `famID2`
- `right2a` has variables `caseID`, `lastName`, `income`, `famID2`
- `right2b` has variables `rowID`, `lastName`, `income`, `famID`

Please see the [merge gallery](https://docs.google.com/spreadsheets/d/1qpofZDygSTUeb0go7DUwsx-Y6gAr2C2WnAXHYbzYU9s/edit?usp=sharing) for more information about the outcomes of different merges. Also note that the default merge strategy in Python is to do an inner join.

| Python Code | Description |
|-------------|-------------|
| <pre>`inner_join = left.merge(right2a, on="caseID", how="inner")`</pre> | inner join (only cases with the same value of the merge by variable are kept) |
| <pre>`left_join = left.merge(right2a, on=["caseID"], how="left")`</pre> | left join (only cases with a defined value of the merge by variable in the left dataframe are kept; cases that were not in the right dataframe originally will have missing values for variables that exist only in the right dataframe) |
| <pre>`right_join = left.merge(right2a, how="right", on="caseID")`</pre> | right join (only cases with a defined value of the merge by variable in the right dataframe are kept; cases that were not in the left dataframe originally will have missing values for variables that exist only in the left dataframe) | |
| <pre>`right_join_sort = left.merge(right2b, how="right", on="famID", sort=True)`</pre> | right join, but sort the output cases by the merge by variable |
| <pre>`outer_join = left.merge(right2a, how="outer", on="caseID")`</pre> | outer join / full join (all cases are kept; cases that were not in one of the dataframes will have missing values for variables that only exist in that dataframe) |
| <pre>`outer_suffix = left.merge(right2b, how="outer", on="famID", suffixes=("_left", "_right"))`</pre> | outer join that demonstrates the ability to customize suffixes added to the names of overlapping variables (the default suffixes are `_x` and `_y` respectively) |
| <pre>`combined = left.combine_first(right)`</pre> | sequential merge, but values which were missing in the left dataframe are updated with the value in the right dataframe (for overlapping variables) |
| <pre>`replaced = left.update(right)`</pre> | replacement merge (for overlapping variables, the value in the right dataframe always takes precedence) |
| <pre>`updated = left.update(right, overwrite=False)`</pre> | update merge (for overlapping variables, use the value in the right dataframe if the value in the left dataframe is missing) |
| <pre>`key_merge = left.merge(right, left_on="caseID", right_on="rowID")`</pre> | Demonstrates a way to specify when the merge by variable has different names in each dataframe |
| <pre>`index_merge = left.merge(right, left_index=True, right_index=True)`</pre> | index merge (instead of a merge by variable, the merge is performed by matching the internal indeces of both dataframes; unlike a sequential merge, overlapping variables are split rather than combined) |

**Rename**

| Python Code | Description |
|-------------|-------------|
| <pre>`df.rename(columns={ "A": "a", "B": "b" }, inplace=True)`</pre> | Rename the variables in place |
| <pre>`df2 = df.rename(columns={ "a": "x", "b": "y" })`</pre> | Rename the variables and store the output in a new dataframe, leaving the original untouched |
| <pre>`df.columns["a"] = "x"`</pre> | Alternate method of renaming a variable in place |
| <pre>`df.columns = ["one", "two"]`</pre> | Rename all variables at once (this demonstrates the need to keep track of variable names internally, since we need to know what the names were before to fill in all the SDTL Rename fields properly) |

**Save**

| Python Code | Description |
|-------------|-------------|
| <pre>`df.to_csv("data.csv")`</pre> | Save an existing dataframe to a CSV file |
| <pre>`df.to_excel("data.xlsx")`</pre> | Save an existing dataframe to an Excel file |
| <pre>`df.to_stata("data.dta")`</pre> | Save an existing dataframe to a Stata file |

**SortCases**

| Python Code | Description |
|-------------|-------------|
| <pre>`df_a = df.sort_values("A")`</pre> | Sort values by a single variable |
| <pre>`df_b = df.sort_values(by="B")`</pre> | Alternate syntax |
| <pre>`df_c = df.sort_values(by=["C"])`</pre> | Both of the above are just syntactic sugar for this method |
| <pre>`df_ab = df.sort_values(by=["A", "B"])`</pre> | Sort by multiple variables |
| <pre>`df_des = df.sort_values(["A", "B"], ascending=False)`</pre> | Sort by multiple variables, descending |
| <pre>`df_list = df.sort_values(by=["A", "B"], ascending=[False, True])`</pre> | Sort by multiple variables, with the first descending and the second ascending |
| <pre>`df_na = df.sort_values(by="C", na_position="first")`</pre> | Specify where missing values should appear in the sort order (there is no SDTL equivalent, so this syntax is parsed with that option ignored) |
| <pre>`df.sort_values(["A", "B", "C"], inplace=True, ascending=[False, True, False], na_position="first")`</pre> | Sort by multiple variables; in-place; descending, ascending, descending; missing values appear at the top |

# Project Development

The following operations are fully supported by the parser:

- loading data from an external data file (.csv, .xlsx, .sas7bdat, or .dta) into a dataframe
- saving a dataframe to an external data file (.csv, .xlsx, or .dta)
- inline comments and docstrings
- dropping/keeping variables from a dataframe
- renaming variables in a dataframe
- appending multiple dataframes
- merging 2 dataframes
- sorting a dataframe
- computing new variables / overwriting existing variables

Support for the following is in progress:

- Function library integration, indlucing parsing assignments that call functions

These operations are largely understood, but not implemented yet:

- GroupBy / summarization

More research is needed:

- Filtering / conditional statements
- Categorical variables
- merging more than 2 dataframes
- to what extent DataFrame.join() is relevant

Out of scope:

- reshape
- sorting variables
- dataframes with more than 2 dimensions
