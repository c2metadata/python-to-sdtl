"""
Class definitions for the SDTL objects, documented at

    http://c2metadata.gitlab.io/sdtl-docs/master/

Copyright 2020 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2020 by Alexander Mueller.
"""

import json

# Expression Base Classes

class ExpressionBase():
    def __init__(self, subtype):
        self.type = subtype

class VariableReferenceBase(ExpressionBase):
    def __init__(self, subtype):
        super().__init__(subtype)

# General Utility Classes

class DataframeDescription():
    def __init__(self, name=None, variables=[], row_dim=[], col_dim=[]):
        self.dataframeName = name
        self.variableInventory = variables.copy()
        self.rowDimensions = row_dim.copy()
        self.columnDimensions = col_dim.copy()
    def add_variable(self, variable):
        self.variableInventory.append(variable)
    def add_row_dim(self, dimension):
        self.rowDimensions.append(dimension)
    def add_col_dim(self, dimension):
        self.columnDimensions.append(dimension)

class FunctionArgument():
    def __init__(self, index, value=None):
        self.type = "FunctionArgument"
        self.argumentName = f"EXP{index}"   # one-indexed
        self.argumentValue = value

class Program():
    def __init__(self, commands=[]):
        self.commands = commands.copy()
    def add_command(self, command):
        self.commands.append(command) 

class SourceInformation():
    def __init__(self, first, last, first_char, last_char, original, processed=None):
        self.type = "SourceInformation"
        self.lineNumberStart = first        # first line of code, inclusive
        self.lineNumberEnd = last           # last line of code, inclusive
        self.sourceStartIndex = first_char  # first character of code, inclusive
        self.sourceStopIndex = last_char    # last character of code, inclusive
        self.originalSourceText = original
        self.processedSourceText = processed

class ValueLabel():
    def __init__(self, value, label):
        self.type = "ValueLabel"
        self.value = value
        self.label = label

# Command Base Classes

class CommandBase():
    isCommandList = False
    def __init__(self, command, message=None):
        self.type = command
        self.command = command
        self.sourceInformation = []
        self.messageText = None
        if message:
            if isinstance(message, list):
                self.messageText = message
            else:
                self.messageText = [message]
    def add_message(self, message):
        if self.messageText:
            self.messageText.append(message)
        else:
            self.messageText = [message]

class InformBase(CommandBase):
    def __init__(self, command, message=None):
        super().__init__(command, message=message)

class TransformBase(CommandBase):
    def __init__(self, command):
        super().__init__(command)
        self.consumesDataframe = []
        self.producesDataframe = []
    def consume(self, consumed):
        if isinstance(consumed, list):
            self.consumesDataframe = consumed.copy()
        else:
            self.consumesDataframe.append(consumed)
    def produce(self, produced):
        if isinstance(produced, list):
            self.producesDataframe = produced.copy()
        else:
            self.producesDataframe.append(produced)
    def produce_consumes(self):
        self.producesDataframe = self.consumesDataframe.copy()

# Informational Commands

class Analysis(InformBase):
    def __init__(self, message=None):
        super().__init__("Analysis", message=message)

class Comment(InformBase):
    def __init__(self, text=None):
        super().__init__("Comment")
        self.commentText = text

class Invalid(InformBase):
    def __init__(self, message):
        super().__init__("Invalid")
        self.problemDescription = message

class Message(InformBase):
    def __init__(self, message, severity=None, lineno=None, charpos=None):
        super().__init__("Message", message=message)
        self.severity = severity
        self.lineNumber = lineno
        self.characterPosition = charpos

class NoTransformOp(InformBase):
    def __init__(self):
        super().__init__("NoTransformOp")

class Unsupported(InformBase):
    def __init__(self, message):
        super().__init__("Unsupported", message=message)

# Expression Types

class BooleanConstantExpression(ExpressionBase):
    def __init__(self, value):
        super().__init__("BooleanConstantExpression")
        self.booleanValue = value

class DateTimeConstant(ExpressionBase):
    def __init__(self, value):
        super().__init__("DateTimeConstant")
        self.dateTime = value

class FunctionCallExpression(ExpressionBase):
    def __init__(self, name=None, n_args=None, arguments=[]):
        super().__init__("FunctionCallExpression")
        self.function = name
        self.isSdtlName = True
        self.arguments = arguments.copy()
        if n_args:
            for index in range(n_args):
                self.arguments.append(FunctionArgument(index + 1))
    def add_argument(self, value):
        argument = FunctionArgument(len(self.arguments) + 1, value)
        self.arguments.append(argument)
    def setArgumentValue(self, index, value):
        self.arguments[index - 1].argumentValue = value
    def makeUserDefined(self):
        self.isSdtlName = False

class GroupedExpression(ExpressionBase):
    def __init__(self, expression):
        super().__init__("GroupedExpression")
        self.expression = expression

class IteratorSymbolExpression(ExpressionBase):
    def __init__(self, name):
        super().__init__("IteratorSymbolExpression")
        self.name = name

class MissingValueConstantExpression(ExpressionBase):
    def __init__(self, value=None):
        super().__init__("MissingValueConstantExpression")
        self.value = value

class NumberRangeExpression(ExpressionBase):
    def __init__(self, start, end, increment=None):
        super().__init__("NumberRangeExpression")
        self.numberRangeStart = str(start)
        self.numberRangeEnd = str(end)
        self.numberRangeIncrement = str(increment) if increment else None

class NumericConstantExpression(ExpressionBase):
    def __init__(self, value):
        super().__init__("NumericConstantExpression")
        #self.value = str(value)
        if float(int(value)) == float(value):
            self.value = str(int(value))
            self.numericType = "Integer"
        else:
            self.value = str(value)
            self.numericType = "Real"

class NumericMaximumValueExpression(ExpressionBase):
    def __init__(self):
        super().__init__("NumericMaximumValueExpression")

class NumericMinimumValueExpression(ExpressionBase):
    def __init__(self):
        super().__init__("NumericMinimumValueExpression")

class StringConstantExpression(ExpressionBase):
    def __init__(self, value):
        super().__init__("StringConstantExpression")
        self.value = value

class StringRangeExpression(ExpressionBase):
    def __init__(self, start, end):
        super().__init__("StringRangeExpression")
        self.rangeStart = start
        self.rangeEnd = end

class TimeDurationConstant(ExpressionBase):
    def __init__(self, duration):
        super().__init__("TimeDurationConstant")
        self.duration = duration

class ValueListExpression(VariableReferenceBase):
    def __init__(self, values):
        super().__init__("ValueListExpression")
        self.values = values

# Variable Types

class AllNumericVariablesExpression(VariableReferenceBase):
    def __init__(self):
        super().__init__("AllNumericVariablesExpression")

class AllTextVariablesExpression(VariableReferenceBase):
    def __init__(self):
        super().__init__("AllTextVariablesExpression")

class AllVariablesExpression(VariableReferenceBase):
    def __init__(self):
        super().__init__("AllVariablesExpression")

class CompositeVariableNameExpression(VariableReferenceBase):
    def __init__(self, name_expression):
        super().__init__("CompositeVariableNameExpression")
        self.computedVariableName = name_expression

class VariableListExpression(VariableReferenceBase):
    def __init__(self, variables=[]):
        super().__init__("VariableListExpression")
        self.variables = []
        for variable in variables:
            self.variables.append(VariableSymbolExpression(variable))
    def add_variable(self, variable):
        self.variables.append(VariableSymbolExpression(variable))
    def add_variables(self, variables):
        for variable in variables:
            self.variables.append(VariableSymbolExpression(variable))

class VariableRangeExpression(VariableReferenceBase):
    def __init__(self, first, last):
        super().__init__("VariableRangeExpression")
        self.first = first
        self.last = last

class VariableSymbolExpression(VariableReferenceBase):
    def __init__(self, name):
        super().__init__("VariableSymbolExpression")
        self.variableName = name

# Specific Utility Classes

class AppendFileDescription():
    def __init__(self, name, drops=[]):
        self.type = "AppendFileDescription"
        self.fileName = name
        self.dropVariables = [
            VariableSymbolExpression(variable)
            for variable in drops
        ]

class IteratorDescription():
    def __init__(self, name, values=[]):
        self.type = "IteratorDescription"
        self.iteratorSymbolName = IteratorSymbolExpression(name)
        self.iteratorValues = values.copy()
    def add_value(self, value):
        self.iteratorValues.append(value)

class MergeFileDescription():
    def __init__(self, name, merge_type=None, update=None, new_row=None, renames=[], drops=[], merge_by_names=[]):
        self.type = "MergeFileDescription"
        self.fileName = name
        self.mergeType = merge_type
        self.update = update
        self.newRow = new_row
        self.renameVariables = []
        for rename in renames:
            if not rename.oldVariable.variableName == rename.newVariable.variableName:
                self.renameVariables.append(rename)
        self.dropVariables = drops.copy()
        self.mergeByNames = merge_by_names.copy()

class RenamePair():
    def __init__(self, old_name, new_name):
        self.type = "RenamePair"
        self.oldVariable = VariableSymbolExpression(old_name)
        self.newVariable = VariableSymbolExpression(new_name)

class ReshapeItemDescription():
    def __init__(self, target_name=None, source_vars=[], stub=None, indeces=[], index_name=None):
        self.type = "ReshapeItemDescription"
        self.targetVariableName = target_name
        self.sourceVariables = source_vars.copy()
        self.stub = stub
        self.indexValues = indeces.copy()
        self.indexVariableName = index_name

class SortCriterion():
    def __init__(self, variable, direction):
        self.type = "SortCriterion"
        self.variable = VariableSymbolExpression(variable)
        self.sortDirection = direction

class ValueLabel():
    def __init__(self, value, label):
        self.type = "ValueLabel"
        self.value = value
        self.label = label

class Weight():
    def __init__(self, variable, type):
        self.type = "Weight"
        self.weightVariable = VariableSymbolExpression(variable)
        self.weightType = type

# Specific Command Classes

class AppendDatasets(TransformBase):
    def __init__(self, files=[]):
        super().__init__("AppendDatasets")
        self.appendFiles = files.copy()
    def append_file(self, file, drops=[]):
        self.appendFiles.append(AppendFileDescription(file, drops))

class Collapse(TransformBase):
    def __init__(self, name=None, groupby=[], agg_vars=[], weight=None):
        super().__init__("Collapse")
        self.outputDatasetName = name
        self.groupByVariables = groupby.copy()
        self.aggregateVariables = agg_vars.copy()
        self.weighting = weight
    def groupby(self, variable=None, variables=[]):
        if variable:
            self.groupByVariables.append(VariableSymbolExpression(variable))
        elif variables:
            self.groupByVariables = [VariableSymbolExpression(variable) for variable in variables]
    def aggregate(self, compute=None, computes=[]):
        if compute:
            self.aggregateVariables.append(compute)
        elif computes:
            for comp in computes:
                self.aggregateVariables.append(comp)

class Compute(TransformBase):
    def __init__(self, variable=None, expression=None, command=True):
        super().__init__("Compute")
        self.variable = variable
        self.expression = expression
        if not command:
            self.command = None

class DoIf(TransformBase):
    def __init__(self, condition, then_commands=[], else_commands=[]):
        super().__init__("DoIf")
        self.condition = condition
        self.thenCommands = then_commands.copy()
        self.elseCommands = else_commands.copy()
    def add_then(self, command):
        self.thenCommands.append(command)
    def add_else(self, command):
        self.elseCommands.append(command)

class DropCases(TransformBase):
    def __init__(self, condition):
        super().__init__("DropCases")
        self.condition = condition

class DropVariables(TransformBase):
    def __init__(self, variables=[]):
        super().__init__("DropVariables")
        self.variables = variables.copy()
    def add_variable(self, variable):
        self.variables.append(VariableSymbolExpression(variable))

class IfRows(TransformBase):
    def __init__(self, condition, then_commands=[], else_commands=[]):
        super().__init__("IfRows")
        self.condition = condition
        self.thenCommands = then_commands.copy()
        self.elseCommands = else_commands.copy()
    def add_then(self, command):
        self.thenCommands.append(command)
    def add_else(self, command):
        self.elseCommands.append(command)

class KeepCases(TransformBase):
    def __init__(self, condition):
        super().__init__("KeepCases")
        self.condition = condition

class KeepVariables(TransformBase):
    def __init__(self, variables=[]):
        super().__init__("KeepVariables")
        self.variables = variables.copy()
    def add_variable(self, variable):
        self.variables.append(VariableSymbolExpression(variable))

class Load(TransformBase):
    def __init__(self, name, software=None, file_format=None, compressed=None):
        super().__init__("Load")
        self.fileName = name
        self.software = software
        self.fileFormat = file_format
        self.isCompressed = compressed

class LoopOverList(TransformBase):
    def __init__(self, iterators=[], commands=[]):
        super().__init__("LoopOverList")
        self.iterators = iterators.copy()
        self.commands = commands.copy()
        self.updated = False
    def add_iterator(self, iterator):
        self.iterators.append(iterator)
    def add_command(self, command):
        self.commands.append(command)

class LoopWhile(TransformBase):
    def __init__(self, condition=None, end_condition=None, commands=[]):
        super().__init__("LoopWhile")
        self.condition = condition
        self.endCondition = end_condition
        self.commands = commands.copy()
        self.updated = False
    def add_command(self, command):
        self.commands.append(command)

class MergeDatasets(TransformBase):
    def __init__(self, files=[], variables=None):
        super().__init__("MergeDatasets")
        self.mergeFiles = files.copy()
        self.mergeByVariables = variables
    def merge_file(self, file):
        self.mergeFiles.append(file)
    def merge_by(self, variables):
        self.mergeByVariables = variables

class Rename(TransformBase):
    def __init__(self, renames=[]):
        super().__init__("Rename")
        self.renames = renames.copy()
    def add_pair(self, old_name, new_name):
        self.renames.append(RenamePair(old_name, new_name))

class ReshapeLong(TransformBase):
    def __init__(self, items=[], case=None, ids=[], drops=[], keeps=[], keep_null=None, count_by_id=None):
        super().__init__("ReshapeLong")
        self.makeItems = items.copy()
        self.caseNumberVariable = case
        self.iDVariables = ids.copy()
        self.dropVariables = drops.copy()
        self.keepVariables = keeps.copy()
        self.keepNullCases = keep_null
        self.countById = count_by_id

class Save(TransformBase):
    def __init__(self, name, software=None, file_format=None, compressed=None):
        super().__init__("Save")
        self.fileName = name
        self.software = software
        self.fileFormat = file_format
        self.isCompressed = compressed

class SetDatasetProperty(TransformBase):
    def __init__(self, name, value):
        super().__init__("SetDatasetProperty")
        self.propertyName = name
        self.value = value

class SetDataType(TransformBase):
    def __init__(self, variables, data_type, subtype_schema=None, subtype=None):
        super().__init__("SetDataType")
        self.variables = variables.copy()
        self.dataType = data_type
        self.subTypeSchema = subtype_schema
        self.subType = subtype

class SetValueLabels(TransformBase):
    def __init__(self, variables=[], labels=[]):
        super().__init__("SetValueLabels")
        self.variables = variables.copy()
        self.labels = labels.copy()

class SortCases(TransformBase):
    def __init__(self, criteria=[]):
        super().__init__("SortCases")
        self.sortCriteria = criteria.copy()
    def add_criterion(self, variable, direction):
        self.sortCriteria.append(SortCriterion(variable, direction))

# Wrapper Classes to Simplify Parsing (not in the SDTL spec)

class CommandList(CommandBase):
    isCommandList = True
    def __init__(self, commands=[]):
        super().__init__("CommandList")
        self.commands = commands.copy()

class PlaceHolder(TransformBase):
    def __init__(self, expression, command=None, **kwargs):
        super().__init__("PlaceHolder")
        self.expression = expression
        self.command = command
        self.keywords = kwargs

def clean_object(sdtl_object):
    if isinstance(sdtl_object, list):
        return [ clean_object(x) for x in sdtl_object if x is not None ]
    elif type(sdtl_object) in (int, float, bool, str):
        return sdtl_object
    elif isinstance(sdtl_object, dict):
        dirty_dict = sdtl_object
    else:
        dirty_dict = vars(sdtl_object)
    clean_dict = {}
    for key, value in dirty_dict.items():
        if isinstance(value, bool) or value:
            if key == "type":
                clean_dict["$type"] = clean_object(value)
            else:
                clean_dict[key] = clean_object(value)
    return clean_dict

def get_json(sdtl_object, indent=None):
    if indent:
        return json.dumps(clean_object(sdtl_object), indent=indent)
    return json.dumps(clean_object(sdtl_object))

"""
# recursively remove any None values or empty lists from an object
def clean_object(obj):
    if isinstance(obj, list):
        return [ clean_object(x) for x in obj if x is not None ]
    elif isinstance(obj, dict):
        clean_dict = {}
        for key, value in obj.items():
            if isinstance(value, bool) or value:
                if key == "type":
                    clean_dict["$type"] = clean_object(value)
                else:
                    clean_dict[key] = clean_object(value)
        return clean_dict
    else:
        return obj
"""

"""
def get_json(sdtl_object, indent=None):
    if indent:
        return json.dumps(sdtl_object, default=lambda o: clean_object(getattr(o, "__dict__", str(o))), indent=indent)
    return json.dumps(sdtl_object, default=lambda o: clean_object(getattr(o, "__dict__", str(o))))
"""