import pandas as pd

strings = pd.read_csv("stringtest.csv")

strings['combined'] = strings.first + strings.second


strings['d_pos'] = strings.combined.str.find("d")

strings['len_first'] = strings.first.str.len()
strings['len_second'] = strings.second.str.len()
strings['len_combined'] = strings.combined.str.len()

strings['len_trim'] = strings.first.str.strip().str.len()
strings['len_rtrim'] = strings.first.str.rstrip().str.len()
strings['len_ltrim'] = strings.first.str.lstrip().str.len() 

strings['sub'] = strings.combined.str.slice(start=4, stop=7)
strings["sub_op"] = strings["combined"][4:7]

strings['repl'] = strings.combined.str.replace("no", 'XX')

strings.to_csv("stringafter.csv")

