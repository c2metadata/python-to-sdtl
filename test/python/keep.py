import pandas as pd

"""
    A B C
--+------
0 | 1 4 7
1 | 2 5 8
2 | 3 6 9
"""
df = pd.read_csv("keep.csv")

kept = df[["A", "B"]]

kept.to_csv("keep.csv")