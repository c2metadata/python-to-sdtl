import pandas as pd


temps = pd.read_csv("temps.csv")

temps_d = temps.assign(celsius=temps["fahrenheit"] - 32)

print(temps_d)