import pandas as pd
import numpy as np

"""
    A B C
--+--------
0 | 3 4 5
1 | 3 2 2
2 | 1 1 NaN
3 | 2 5 9
4 | 4 3 4
"""
df = pd.read_csv("df.csv")

df_a = df.sort_values("A")
df_b = df.sort_values(by="B")
df_c = df.sort_values(by=["C"])

df_ab = df.sort_values(by=["A", "B"])
df_des = df.sort_values(["A", "B"], ascending=False)
df_list = df.sort_values(by=["A", "B"], ascending=[False, True])
df_col = df.sort_values(0, axis="columns")

df_na = df.sort_values(by="C", na_position="first")

df.sort_values(["A", "B", "C"], inplace=True, ascending=[False, True, False], na_position="first")

df.to_csv("df.csv")