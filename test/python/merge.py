import pandas as pd

"""
left
    caseID firstName income famID
--+------------------------------
0 | 1      Abe       15000  11
1 | 2      Barry     None   11
2 | 3      Chuck     17000  12
3 | 4      Dave      25000  13
4 | 5      Ed        21000  14

right
    rowID  lastName  income famID2
--+-------------------------------
0 | 1      Lincoln   31000  11
1 | 2      Madison   32000  12
2 | 3      Nelson    33000  13
3 | 6      Quincy    34000  14
"""
left = pd.read_csv("left.csv")
right = pd.read_csv("right.csv")
right2a = right.rename(columns={"rowID": "caseID"})
right2b = right.rename(columns={"famID2": "famID"})

default = left.merge(right)		# This results in an empty dataframe

inner_join = left.merge(right2a, on="caseID", how="inner")
left_join = left.merge(right2a, on=["caseID"], how="left")
right_join = left.merge(right2a, how="right", on="caseID")	# default suffixes
right_join_no_by = left.merge(right, how="right")		# This is unsupported
right_join_sort = left.merge(right2b, how="right", on="famID", sort=True)
outer_join = left.merge(right2a, how="outer", on="caseID")
outer_suffix = left.merge(right2b, how="outer", on="famID", suffixes=("_left", "_right"))

flag_merge = left.merge(right2a, indicator=True)
flag_string = left.merge(right2a, indicator="Source")

combined = left.combine_first(right)
replaced = left.update(right)
updated = left.update(right, overwrite=False)

key_merge = left.merge(right, left_on="caseID", right_on="rowID")
index_merge = left.merge(right, left_index=True, right_index=True)