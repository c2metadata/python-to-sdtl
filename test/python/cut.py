import pandas as pd

cdata = pd.read_csv("GSS_sample.csv")

cdata = cdata.assign(age_rec = pd.cut(cdata['AGE'], [0, 15, 30, 50, 999], include_lowest=True, labels=['0-14', '15-29', '30-49', '50+'] ))
cdata = cdata.assign(age_rec2 = pd.cut(ctata.AGE, 3, precision=4))
cdata["age_rec3"] = pd.cut(cdata['AGE'], [0, 15, 30, 50, 999], right=False, labels=['0-14', '15-29', '30-49', '50+'] )

cdata["age_rec4"] = pd.qcut(cdata["AGE"], 4)
cdata["age_rec5"] = pd.qcut(cdata.AGE, [0, .25, .5, .75, 1.])

cdata.to_csv("GSS_sample_after.csv")