"""This is a single-line docstring"""

#This is a single-line comment

"""
This
is
a
multi-
line
docstring
"""

"docstring with one set of quotes"

'docstring with one set of single-quotes'

'''docstring with three single-quotes'''

'''multi-
l
i
n
e
docstring with
three single quotes'''