import pandas as pd
import io
import os

os.chdir(r'C:\\Users\\altergc\\Documents\\ICPSR\\Project development\\metadata_capture\\SDTL\\cut')

cdata = pd.read_csv("cut_test.csv")

cdata = cdata.assign( cut1 = pd.cut(cdata.varX, [-999, 3, 9, 999]) )
cdata = cdata.assign( cut1y = pd.cut(cdata.varX, [-999, 3, 9, 999], include_lowest=True) )

cdata = cdata.assign( cut1a = pd.cut(cdata.varX, [-999, 3, 9, 999], right=False) )
cdata = cdata.assign( cut1b = pd.cut(cdata.varX, [-999, 3, 9, 999], right=False, labels=['low', 'med', 'hi'] )  )

cdata = cdata.assign( cut1c = pd.cut(cdata.varX, [ 1, 3, 9, 999]) )
cdata = cdata.assign( cut1d = pd.cut(cdata.varX, [ 1, 3, 9, 999], include_lowest=True) )

cdata = cdata.assign( cut2 = pd.cut(cdata.varX, 3) )
cdata = cdata.assign( cut2a = pd.cut(cdata.varX, 3, labels=False) )

cdata = cdata.assign( cut3 = pd.qcut(cdata.varX, 3) )

cdata = cdata.assign( cut3a = pd.qcut(cdata.varX, [0, .4, .8, 1] ) )


cdata = cdata.assign( cut4a = pd.cut(cdata.varX, [-999, 3, 9, 999], include_lowest=True, right=False) )


print(cdata[['varX','cut1','cut1y','cut1a', 'cut1b','cut1c', 'cut1d']])

print(cdata[['varX','cut2','cut2a', 'cut3','cut3a', 'cut4a']])

print(cdata[['cut1y', 'cut4a']])

cdata = cdata.assign( cut1u = pd.cut(cdata.varX, [-999, 3, 9, 999], ordered=False, labels=['A', 'B', 'A'] )  )

      
