Module(
    body=[
        Import(
            names=[alias(name='pandas', asname='pd')],
        ),
        Assign(
            targets=[Name(id='df', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='pd', ctx=Load()),
                    attr='read_csv',
                    ctx=Load(),
                ),
                args=[Constant(value='df.csv', kind=None)],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='case', ctx=Store())],
            value=Subscript(
                value=Name(id='df', ctx=Load()),
                slice=Compare(
                    left=Subscript(
                        value=Name(id='df', ctx=Load()),
                        slice=Constant(value='A', kind=None),
                        ctx=Load(),
                    ),
                    ops=[Gt()],
                    comparators=[Constant(value=2, kind=None)],
                ),
                ctx=Load(),
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='case2', ctx=Store())],
            value=Subscript(
                value=Name(id='df', ctx=Load()),
                slice=BinOp(
                    left=Compare(
                        left=Subscript(
                            value=Name(id='df', ctx=Load()),
                            slice=Constant(value='A', kind=None),
                            ctx=Load(),
                        ),
                        ops=[Lt()],
                        comparators=[Constant(value=4, kind=None)],
                    ),
                    op=BitAnd(),
                    right=UnaryOp(
                        op=Invert(),
                        operand=Compare(
                            left=Subscript(
                                value=Name(id='df', ctx=Load()),
                                slice=Constant(value='B', kind=None),
                                ctx=Load(),
                            ),
                            ops=[Eq()],
                            comparators=[Constant(value=3, kind=None)],
                        ),
                    ),
                ),
                ctx=Load(),
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='case3', ctx=Store())],
            value=Subscript(
                value=Name(id='df', ctx=Load()),
                slice=Call(
                    func=Attribute(
                        value=Call(
                            func=Attribute(
                                value=Name(id='df', ctx=Load()),
                                attr='isin',
                                ctx=Load(),
                            ),
                            args=[
                                List(
                                    elts=[
                                        Constant(value=2, kind=None),
                                        Constant(value=3, kind=None),
                                    ],
                                    ctx=Load(),
                                ),
                            ],
                            keywords=[],
                        ),
                        attr='all',
                        ctx=Load(),
                    ),
                    args=[],
                    keywords=[
                        keyword(
                            arg='axis',
                            value=Constant(value=1, kind=None),
                        ),
                    ],
                ),
                ctx=Load(),
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='case4', ctx=Store())],
            value=Subscript(
                value=Name(id='df', ctx=Load()),
                slice=Call(
                    func=Attribute(
                        value=Call(
                            func=Attribute(
                                value=Name(id='df', ctx=Load()),
                                attr='isna',
                                ctx=Load(),
                            ),
                            args=[],
                            keywords=[],
                        ),
                        attr='any',
                        ctx=Load(),
                    ),
                    args=[],
                    keywords=[
                        keyword(
                            arg='axis',
                            value=Constant(value=1, kind=None),
                        ),
                    ],
                ),
                ctx=Load(),
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='case5', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='df', ctx=Load()),
                    attr='dropna',
                    ctx=Load(),
                ),
                args=[],
                keywords=[
                    keyword(
                        arg='how',
                        value=Constant(value='all', kind=None),
                    ),
                ],
            ),
            type_comment=None,
        ),
    ],
    type_ignores=[],
)