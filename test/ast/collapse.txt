Module(
    body=[
        Import(
            names=[alias(name='pandas', asname='pd')],
        ),
        Assign(
            targets=[Name(id='scores', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='pd', ctx=Load()),
                    attr='read_csv',
                    ctx=Load(),
                ),
                args=[Constant(value='scores.csv', kind=None)],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='mean', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='scores', ctx=Load()),
                    attr='mean',
                    ctx=Load(),
                ),
                args=[],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='desc', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='scores', ctx=Load()),
                    attr='describe',
                    ctx=Load(),
                ),
                args=[],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='median_age', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Call(
                        func=Attribute(
                            value=Name(id='scores', ctx=Load()),
                            attr='groupby',
                            ctx=Load(),
                        ),
                        args=[Constant(value='Age', kind=None)],
                        keywords=[],
                    ),
                    attr='median',
                    ctx=Load(),
                ),
                args=[],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='group_sum', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Call(
                        func=Attribute(
                            value=Name(id='scores', ctx=Load()),
                            attr='groupby',
                            ctx=Load(),
                        ),
                        args=[
                            List(
                                elts=[
                                    Constant(value='Gender', kind=None),
                                    Constant(value='Age', kind=None),
                                ],
                                ctx=Load(),
                            ),
                        ],
                        keywords=[],
                    ),
                    attr='sum',
                    ctx=Load(),
                ),
                args=[],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='group_des', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Call(
                        func=Attribute(
                            value=Name(id='scores', ctx=Load()),
                            attr='groupby',
                            ctx=Load(),
                        ),
                        args=[Constant(value='Gender', kind=None)],
                        keywords=[],
                    ),
                    attr='describe',
                    ctx=Load(),
                ),
                args=[],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='non_index', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Call(
                        func=Attribute(
                            value=Name(id='scores', ctx=Load()),
                            attr='groupby',
                            ctx=Load(),
                        ),
                        args=[Constant(value='Gender', kind=None)],
                        keywords=[
                            keyword(
                                arg='as_index',
                                value=Constant(value=False, kind=None),
                            ),
                        ],
                    ),
                    attr='mad',
                    ctx=Load(),
                ),
                args=[],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='single_agg', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Call(
                        func=Attribute(
                            value=Name(id='scores', ctx=Load()),
                            attr='groupby',
                            ctx=Load(),
                        ),
                        args=[Constant(value='Gender', kind=None)],
                        keywords=[],
                    ),
                    attr='agg',
                    ctx=Load(),
                ),
                args=[Constant(value='var', kind=None)],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='multiple_agg', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Call(
                        func=Attribute(
                            value=Name(id='scores', ctx=Load()),
                            attr='groupby',
                            ctx=Load(),
                        ),
                        args=[Constant(value='Gender', kind=None)],
                        keywords=[],
                    ),
                    attr='agg',
                    ctx=Load(),
                ),
                args=[
                    List(
                        elts=[
                            Constant(value='quantile', kind=None),
                            Constant(value='nunique', kind=None),
                            Constant(value='skew', kind=None),
                        ],
                        ctx=Load(),
                    ),
                ],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='named_agg', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Call(
                        func=Attribute(
                            value=Name(id='scores', ctx=Load()),
                            attr='groupby',
                            ctx=Load(),
                        ),
                        args=[Constant(value='Gender', kind=None)],
                        keywords=[],
                    ),
                    attr='agg',
                    ctx=Load(),
                ),
                args=[],
                keywords=[
                    keyword(
                        arg='youngest',
                        value=Call(
                            func=Attribute(
                                value=Name(id='pd', ctx=Load()),
                                attr='NamedAgg',
                                ctx=Load(),
                            ),
                            args=[],
                            keywords=[
                                keyword(
                                    arg='column',
                                    value=Constant(value='Age', kind=None),
                                ),
                                keyword(
                                    arg='aggfunc',
                                    value=Constant(value='min', kind=None),
                                ),
                            ],
                        ),
                    ),
                    keyword(
                        arg='oldest',
                        value=Call(
                            func=Attribute(
                                value=Name(id='pd', ctx=Load()),
                                attr='NamedAgg',
                                ctx=Load(),
                            ),
                            args=[],
                            keywords=[
                                keyword(
                                    arg='column',
                                    value=Constant(value='Age', kind=None),
                                ),
                                keyword(
                                    arg='aggfunc',
                                    value=Constant(value='max', kind=None),
                                ),
                            ],
                        ),
                    ),
                    keyword(
                        arg='mean_score',
                        value=Call(
                            func=Attribute(
                                value=Name(id='pd', ctx=Load()),
                                attr='NamedAgg',
                                ctx=Load(),
                            ),
                            args=[],
                            keywords=[
                                keyword(
                                    arg='column',
                                    value=Constant(value='Score', kind=None),
                                ),
                                keyword(
                                    arg='aggfunc',
                                    value=Constant(value='mean', kind=None),
                                ),
                            ],
                        ),
                    ),
                ],
            ),
            type_comment=None,
        ),
    ],
    type_ignores=[],
)