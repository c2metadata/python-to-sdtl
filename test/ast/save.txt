Module(
    body=[
        Expr(
            value=Constant(value='Test the various methods of writing a DataFrame to a file.', kind=None),
        ),
        Import(
            names=[alias(name='pandas', asname='pd')],
        ),
        Assign(
            targets=[Name(id='df', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='pd', ctx=Load()),
                    attr='read_csv',
                    ctx=Load(),
                ),
                args=[Constant(value='df.csv', kind=None)],
                keywords=[],
            ),
            type_comment=None,
        ),
        Expr(
            value=Call(
                func=Attribute(
                    value=Name(id='df', ctx=Load()),
                    attr='to_csv',
                    ctx=Load(),
                ),
                args=[Constant(value='data.csv', kind=None)],
                keywords=[],
            ),
        ),
        Expr(
            value=Call(
                func=Attribute(
                    value=Name(id='df', ctx=Load()),
                    attr='to_excel',
                    ctx=Load(),
                ),
                args=[Constant(value='data.xlsx', kind=None)],
                keywords=[],
            ),
        ),
        Expr(
            value=Call(
                func=Attribute(
                    value=Name(id='df', ctx=Load()),
                    attr='to_stata',
                    ctx=Load(),
                ),
                args=[Constant(value='data.dta', kind=None)],
                keywords=[],
            ),
        ),
    ],
    type_ignores=[],
)