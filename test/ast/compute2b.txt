Module(
    body=[
        Import(
            names=[alias(name='pandas', asname='pd')],
        ),
        Assign(
            targets=[Name(id='temps', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='pd', ctx=Load()),
                    attr='read_csv',
                    ctx=Load(),
                ),
                args=[Constant(value='temps.csv', kind=None)],
                keywords=[],
            ),
            type_comment=None,
        ),
        Assign(
            targets=[Name(id='temps_d', ctx=Store())],
            value=Call(
                func=Attribute(
                    value=Name(id='temps', ctx=Load()),
                    attr='assign',
                    ctx=Load(),
                ),
                args=[],
                keywords=[
                    keyword(
                        arg='celsius',
                        value=Subscript(
                            value=Name(id='temps', ctx=Load()),
                            slice=Constant(value='fahrenheit', kind=None),
                            ctx=Load(),
                        ),
                    ),
                ],
            ),
            type_comment=None,
        ),
        Expr(
            value=Call(
                func=Name(id='print', ctx=Load()),
                args=[Name(id='temps_d', ctx=Load())],
                keywords=[],
            ),
        ),
    ],
    type_ignores=[],
)