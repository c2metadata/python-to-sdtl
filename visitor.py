"""
Visitor methods for the Abstract Syntax Tree.

Copyright 2019 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2019 by Alexander Mueller.
"""

import ast
import copy
import io
import os
import tokenize
import traceback

import fn_map

from SDTL import *

LINE_COUNTS = {}   # line number -> character count before that line

def convert_file(filepath, dataframe_variables={}, file_variables={}):
    with open(filepath, "r") as source:
        tree = ast.parse(source.read())
    return parse(tree, dataframe_variables, file_variables, filepath=filepath)

def convert_string(string, dataframe_variables={}, file_variables={}):
    tree = ast.parse(string)
    return parse(tree, dataframe_variables, file_variables, string=string)

def parse(tree, dataframe_variables={}, file_variables={}, filepath=None, string=None):
    count_characters(filepath, string)

    visitor = Visitor(dataframe_variables, file_variables, filepath, string)
    visitor.visit(tree)

    # we need to parse comments seperately because the AST module can't do that
    program = visitor.report()
    comments = get_comments(filepath, string)

    # add all comments to the end of the program, then sort by sourceInformation
    program.commands = program.commands + comments
    program.commands.sort(key=lambda x: (x.sourceInformation[0].lineNumberStart,
                                         x.sourceInformation[0].sourceStartIndex))

    return program

# count the number of characters before each line (including newlines)
# store these value in the global LINE_COUNTS dictionary
def count_characters(filepath, string):
    if filepath is not None:
        file_object = open(filepath, "r")
    else:
        file_object = io.StringIO(string)

    char_count = 0

    for line_number, line in enumerate(file_object):

        # need to add one because SDTL expects one-indexed lines
        LINE_COUNTS[line_number + 1] = char_count

        # add the length of the current line to the overall character count
        # (the line object already includes the newline characters at the end)
        char_count = char_count + len(line)

    file_object.close()

def get_comments(filepath, string):

    comments = []
    if filepath is not None:
        file_object = open(filepath, "r")
    else:
        file_object = io.StringIO(string)
    initial_position = file_object.tell()
    previous_newline = False
    first_line = True

    # iterate through all tokens in the file looking for comments, docstrings, and new lines
    for token_type, token_string, start_tuple, end_tuple, line in tokenize.generate_tokens(file_object.readline):

        # kludge to fix a bug where tokenize skips the first line
        if first_line:
            file_object.seek(initial_position)
            first_line = False

        # if this is an octothorpe / sharp sign / hashtag comment, the comment text shouldn't include the # character
        if token_type == tokenize.COMMENT:
            comment = Comment(token_string[1:])
            comment.sourceInformation = get_comment_source_info(token_string, start_tuple, end_tuple)
            comments.append(comment)
            previous_newline = False

        # if this line starts with a string literal, it must be a docstring
        elif token_type == tokenize.STRING and previous_newline:
            comment = Comment()
            comment.sourceInformation = get_comment_source_info(token_string, start_tuple, end_tuple)

            # if the docstring is declared with triple quotes, the first and last three characters need to be removed
            if ("\"\"\"" in token_string) or ("\'\'\'" in token_string):
                comment.commentText = token_string[3:-3]

            # otherwise, just the first and last characters need removal
            else:
                comment.commentText = token_string[1:-1]

            comments.append(comment)
            previous_newline = False

        # record when the previous token was a new line so that we don't interpret regular strings as comments
        elif token_type == tokenize.NEWLINE or token_type == tokenize.NL:
            previous_newline = True

        else:
            previous_newline = False

    file_object.close()
    return comments

def get_comment_source_info(comment, start_tuple, end_tuple):
    starting_line = start_tuple[0] - 1 # line numbers are wrong because of first line-skipping
    ending_line = end_tuple[0] - 1
    return [
        SourceInformation(
            starting_line,
            ending_line,
            LINE_COUNTS[starting_line] + start_tuple[1] + 1,
            LINE_COUNTS[ending_line] + end_tuple[1],
            comment
        )
    ]

# returns all of left plus any of right that isn't in left
def list_union(left, right, attribute=None):
    if attribute:
        try:
            left_attr = getattr(left, attribute)
            right_attr = getattr(right, attribute)
            union = left_attr.copy()
            left_dict = dict.fromkeys(left_attr.copy())
            for x in right_attr:
                if x not in left_dict:
                    union.append(x)
            return union
        except AttributeError:
            try:
                return getattr(left, attribute)
            except AttributeError:
                try:
                    return getattr(right, attribute)
                except AttributeError:
                    return []
    # using dicts gives us hashing, making this O(n + m) instead of O(n^2)
    union = left.copy()
    left_dict = dict.fromkeys(left.copy())
    for x in right:
        if x not in left_dict:
            union.append(x)
    return union

# returns all elements in both left and right
def list_intersection(left, right):
    intersection = []
    left_dict = dict.fromkeys(left.copy())
    for x in right:
        if x in left_dict:
            intersection.append(x)
    return intersection

# returns all of left except what's in right
def list_difference(left, right):
    difference = []
    right_dict = dict.fromkeys(right.copy())
    for x in left:
        if x not in right_dict:
            difference.append(x)
    return difference

class Visitor(ast.NodeVisitor):
    def __init__(self, dataframe_variables={}, file_variables={}, filepath=None, string=None):
        self.program = Program()
        self.source = filepath      # source filepath, if parser was called with an actual file
        self.string = string        # source string, if parser was called with source in a string
        self.df_count = 0           # counter for the number of currently active dataframes
        self.dataframe_variables = dataframe_variables.copy()
        self.file_variables = file_variables.copy()
        self.lambda_variables = {}
        self.row_dimensions = {}    # map of dataframe -> row MultiIndex (if present)
        self.column_dimensions = {} # map of dataframe -> column MultiIndex (if present)
        self.pandas_alias = "pd"    # the alias under which pandas is referred in the program
        self.numpy_alias = "np"     # the alias under which numpy is referred in the program

    def clearActiveDataframes(self):
        dataframe_dicts = [
            self.dataframe_variables, 
            self.row_dimensions, 
            self.column_dimensions
        ]
        for dataframe_dict in dataframe_dicts:
            for dataframe in list(dataframe_dict.keys()):
                if dataframe.startswith("_activeDataframe_"):
                    dataframe_dict.pop(dataframe)
        self.lambda_variables.clear()
        self.df_count = 0

    def combine_consumes(self, left, right):
        if not left:
            return copy.copy(right)
        if not right:
            return copy.copy(left)
        if isinstance(left, list):
            left_dataframes = dict.fromkeys([dataframe.dataframeName for dataframe in left])
            if isinstance(right, list):
                for dataframe in right:
                    if dataframe.dataframeName not in left_dataframes:
                        left.append(right)
                #left = list_union(left, right)
            elif right.dataframeName not in left_dataframes:
                left.append(right)
            return left
        elif isinstance(right, list):
            right_dataframes = dict.fromkeys([dataframe.dataframeName for dataframe in right])
            if left.dataframeName not in right_dataframes:
                right.insert(0, left)
            return right
        elif left.dataframeName != right.dataframeName:
            return [left, right]
        return left

    def combine_produces(self, left, right):
        if not left:
            return copy.copy(right)
        if not right:
            return copy.copy(left)

        if isinstance(left, list):
            right_dataframe = copy.copy(right)
            for left_dataframe in left:
                right_dataframe = self.combine_produces(left_dataframe, right_dataframe)
            return right_dataframe

        elif isinstance(right, list):
            left_dataframe = copy.copy(left)
            for right_dataframe in right:
                left_dataframe = self.combine_produces(left_dataframe, right_dataframe)
            return left_dataframe

        elif left.dataframeName != right.dataframeName:
            if "_activeDataframe_" not in [left.dataframeName, right.dataframeName]:
                return [left, right]

        dataframe = DataframeDescription(left.dataframeName, 
                                         variables=list_union(left, right, attribute="variableInventory"))
        if left.rowDimensions:
            if right.rowDimensions:
                dataframe.rowDimensions = list_union(left, right, attribute="rowDimensions")
            else:
                dataframe.rowDimensions = left.rowDimensions

        elif right.rowDimensions:
            dataframe.rowDimensions = right.rowDimensions

        if left.columnDimensions:
            if right.columnDimensions:
                dataframe.columnDimensions = list_union(left, right, attribute="columnDimensions")
            else:
                dataframe.columnDimensions = left.columnDimensions

        elif right.columnDimensions:
            dataframe.columnDimensions = right.columnDimensions

        return dataframe

    def consume_and_produce(self, command, dataframe):
        consumed = self.makeDataframeDescription(dataframe)
        produced = self.makeDataframeDescription(None, variables=consumed.variableInventory)

        command.consume(consumed)
        command.produce(produced)

    def copy_dataframes(self, source, target):
        target.consumesDataframe = copy.copy(source.producesDataframe)
        target.producesDataframe = copy.copy(source.producesDataframe)

    def copy_placeholder(self, placeholder, command_type):
        command = command_type(placeholder.expression)
        command.consume(placeholder.consumesDataframe)
        command.produce(placeholder.producesDataframe)
        return command

    def getListFromRange(self, first, last, dataframe):
        first_index = self.dataframe_variables[dataframe].index(first)
        last_index = self.dataframe_variables[dataframe].index(last) + 1
        return self.dataframe_variables[dataframe][first_index:last_index]

    def getDictFromKeywords(self, keywords):
        keyword_dict = {}
        for keyword in keywords:
            keyword_dict[keyword.arg] = keyword.value
        return keyword_dict

    def getSourceString(self, node):
        if self.source is not None:
            with open(self.source, "r") as source:
                source_text = ast.get_source_segment(source.read(), node)
        else:
            source_text = ast.get_source_segment(self.string, node)
        return source_text

    def getSourceInfo(self, node):
        return [
            SourceInformation(
                node.lineno,
                node.end_lineno,
                LINE_COUNTS[node.lineno] + node.col_offset + 1,
                LINE_COUNTS[node.end_lineno] + node.end_col_offset,
                self.getSourceString(node)
            )
        ]

    def makeAppendDatasets(self, dataframes, inner=False):
        variables = []
        for dataframe in dataframes:
            variables = list(dict.fromkeys(variables + self.dataframe_variables[dataframe]))

        df_drops = {dataframe: [] for dataframe in dataframes}
        if inner:
            common_variables = set(self.dataframe_variables[dataframes[0]])
            for dataframe in dataframes:
                common_variables = common_variables & set(self.dataframe_variables[dataframe])
            for index, dataframe in enumerate(dataframes):
                df_variables = self.dataframe_variables[dataframe]
                for variable in df_variables:
                    if variable not in common_variables:
                        df_drops[dataframe].append(variable)
                        variables.remove(variable)

        command = AppendDatasets()

        for dataframe in dataframes:
            command.append_file(dataframe, drops=df_drops[dataframe])

        # TODO: handle the appending of MultiIndexed dataframes
        command.consume(self.makeDataframeDescriptions(dataframes))
        command.produce(DataframeDescription(variables=variables))

        return command

    def makeCases(self, node):
        placeholder = self.visit(node.slice)
        return self.copy_placeholder(placeholder, KeepCases)

    # returns SDTL inner type dict
    def makeDataframeDescription(self, dataframe, variables=None, rowDimensions=None, columnDimensions=None):
        if dataframe:
            description = DataframeDescription(name=dataframe)
        else:
            description = DataframeDescription(name=f"_activeDataframe_{self.df_count}")

        if variables:
            description.variableInventory = variables.copy()
        else:
            try:
                description.variableInventory = self.dataframe_variables[dataframe].copy()
            except KeyError:
                try:
                    description.variableInventory = self.lambda_variables[dataframe].copy()
                except KeyError:
                    pass

        if rowDimensions:
            description.rowDimensions = rowDimensions
        elif dataframe in self.row_dimensions:
            description.rowDimensions = self.row_dimensions[dataframe]

        if columnDimensions:
            description.columnDimensions = columnDimensions
        elif dataframe in self.column_dimensions:
            description.columnDimensions = self.column_dimensions[dataframe]

        return description

    # returns array of SDTL inner types
    def makeDataframeDescriptions(self, consumes):        
        if isinstance(consumes, list):
            return [self.makeDataframeDescription(dataframe) for dataframe in consumes]
        else:
            return [self.makeDataframeDescription(consumes)]

    def makeDrop(self, node, source=None, target=None):
        drop = DropVariables()
        columns = []
        variable_range = None
        if isinstance(node, ast.Call):
            if(len(node.args) > 0):
                # if a list is directly enumerated, just copy it
                try:
                    columns = list(ast.literal_eval(self.getSourceString(node.args[0])))
                # otherwise, this is a variable range that needs expanding
                except ValueError:
                    variable_range = self.makeVariableRange(node.args[0])

            keywords = self.getDictFromKeywords(node.keywords)

            if "columns" in keywords:
                columns = list(ast.literal_eval(self.getSourceString(keywords["columns"])))

            if "inplace" in keywords and keywords["inplace"].value:
                target = source

            if "axis" in keywords and (keywords["axis"].value == 0 or keywords["axis"].value == "index"):
                return Invalid("Deleting rows is not currently supported by the parsing application.")

        # TODO: what happens if no argument is passed for source?
        variables = self.dataframe_variables[source].copy()

        drop.consume(DataframeDescription(source, variables=variables))

        # remove the variables from the data

        # if it's a variable range, find its location, then remove that range
        if variable_range:
            drop.variables = [variable_range]

            try:
                range_start = variables.index(variable_range.first)
            except ValueError:
                return Invalid("The variable range does not start within the scope of variables provided.")
            try:
                # plus one to account for python ranges being exclusive on the right
                range_end = variables.index(variable_range.last) + 1
            except ValueError:
                return Invalid("The variable range does not end within the scope of variables provided.")

            del variables[range_start:range_end]
        
        # otherwise, remove each enumerated variable
        else:
            for variable in columns:
                try:
                    variables.remove(variable)
                    drop.add_variable(variable)
                except ValueError:
                    return Invalid("Trying to drop a variable that doesn't exist in the data.")

        drop.produce(DataframeDescription(target, variables=variables))

        return drop

    # returns array of SDTL inner types
    def makeFunctionArguments(self, expressions):
        return [
            FunctionArgument(index + 1, value)
            for index, value in enumerate(expressions)
        ]

    def makeKeep(self, node):
        keep = KeepVariables()
        source = node.value.id
        variables = self.dataframe_variables[source].copy()
        columns = list(ast.literal_eval(self.getSourceString(node.slice)))

        keep.consume(self.makeDataframeDescription(source))

        # keep the listed variables
        for variable in columns:
            if variable not in variables:
                return Invalid("Trying to keep a variable that doesn't exist in the data.")
            keep.add_variable(variable)

        # TODO: handle variable keep operations on MultiIndexed dataframes
        keep.produce(DataframeDescription(variables=columns))

        return keep

    def makeLibraryFunction(self, node):
        function, dataframe, consumes = (None, None, None)
        python_name = node.func.attr
        positional_args = [self.visit(arg) for arg in node.args]
        keyword_args = {keyword.arg: self.visit(keyword.value) for keyword in node.keywords}

        consumes = []
        for positional in positional_args:
            if isinstance(positional, TransformBase):
                consumes.append(positional.consumesDataframe.copy())
        for keyword, value in keyword_args.items():
            if isinstance(value, TransformBase):
                consumes.append(value.consumesDataframe.copy())
        
        # if this function is being called on a dataframe directly
        if isinstance(node.func.value, ast.Attribute):
            # if this is a string function, append "str." to the python name
            if node.func.value.attr == "str":
                python_name = "str." + python_name
            variable = self.visit(node.func.value.value)
            positional_args.insert(0, variable.expression)
            # TODO: add variable as a keyword argument to certain functions
            try:
                function = fn_map.get_SDTL_function(python_name, positional_args, keyword_args, "horizontal")
                placeholder = PlaceHolder(function)
                if variable.consumesDataframe:
                    placeholder.consumesDataframe = variable.consumesDataframe
                return placeholder
            except KeyError:
                if python_name == "describe":
                    return Analysis()
                else:
                    return Unsupported("Collapse functions which return a Series are not supported.")
        
        # if this is an inner call in a chain of function calls
        elif isinstance(node.func.value, ast.Call):
            collapse = self.visit(node.func.value)
            if isinstance(collapse, Collapse):
                # we need the groupby variables as a regular list
                # in order to exclude them from the aggregate variables
                # WARNING: assumes there are no VariableListExpressions in groupby
                group_vars = [variable.variableName for variable in collapse.groupByVariables]

                # TODO: remove this if we start capturing variable metadata
                numeric = [AllNumericVariablesExpression()]

                agg_vars = []
                for dataframe in collapse.consumesDataframe:
                    for variable in self.dataframe_variables[dataframe.dataframeName]:
                        if variable not in group_vars:
                            agg_vars.append(variable)

                produces = collapse.producesDataframe[0]
                try:
                    # agg is not a collapse function, but a way to specify others
                    if python_name == "agg":
                        if keyword_args:

                            for keyword, value in keyword_args.items():
                                aggfunc = value["aggfunc"].value
                                column = VariableSymbolExpression(value["column"].value)
                                fn_expr = fn_map.get_SDTL_function(aggfunc, [column], {}, "collapse", named_agg=True)
                                compute = Compute(VariableSymbolExpression(keyword), fn_expr, command=False)
                                collapse.aggregate(compute=compute)
                                produces.add_variable(keyword)
                            return collapse

                        elif isinstance(positional_args[0], list):
                            produces.columnDimensions = agg_vars
                            for agg_var in agg_vars:

                                for fn_var in positional_args[0]:
                                    fn_expr = fn_map.get_SDTL_function(fn_var, numeric, keyword_args, "collapse")
                                    variable = f"{agg_var}.{fn_var}"
                                    compute = Compute(VariableSymbolExpression(variable), fn_expr, command=False)
                                    collapse.aggregate(compute=compute)
                                    produces.add_variable(f"{agg_var}.{fn_var}")

                            return collapse

                        else:
                            python_name = positional_args[0].value
                            fn_expr = fn_map.get_SDTL_function(python_name, numeric, keyword_args, "collapse")

                    elif python_name == "describe":
                        return Analysis()

                    else:
                        fn_expr = fn_map.get_SDTL_function(python_name, numeric, keyword_args, "collapse")

                except KeyError as e:
                    traceback.print_exc()
                    return Unsupported("Unknown function.")

                for variable in agg_vars:
                    compute = Compute(VariableSymbolExpression(variable), fn_expr, command=False)
                    collapse.aggregate(compute)

                produces.variableInventory = agg_vars

                return collapse
        
        # if this is the outermost call in a function call chain
        elif isinstance(node.func.value, ast.Name):
            if node.func.value.id in self.dataframe_variables:
                if python_name == "groupby":
                    collapse = Collapse()
                    collapse.add_message("Collapsed variables referring to non-numeric variables will not exist in the data file.")

                    first_arg = positional_args[0]
                    if isinstance(first_arg, list):
                        row_dimensions = first_arg
                        collapse.groupby(variables=first_arg)
                    else:
                        row_dimensions = [first_arg.value]
                        collapse.groupby(variable=first_arg.value)

                    if "as_index" in keyword_args and not keyword_args["as_index"]:
                        produces = DataframeDescription()
                    else:
                        produces = DataframeDescription(row_dim=row_dimensions)

                    collapse.consume(self.makeDataframeDescription(node.func.value.id))
                    collapse.produce(produces)

                    return collapse

                elif python_name == "describe":
                    return Analysis()

                else:
                    try:
                        fn_expr = fn_map.get_SDTL_function(python_name, {}, {}, "collapse")
                        return Unsupported("Collapse functions which return a Series are not supported.")
                    except KeyError:
                        return Unsupported("Unknown Collapse function.")

            elif node.func.value.id == self.pandas_alias:
                if python_name == "NamedAgg":
                    return self.getDictFromKeywords(node.keywords)
                else:
                    return Unsupported("Unknown function.")

            elif node.func.value.id == self.numpy_alias:
                python_name = "numpy." + python_name
                try:
                    function = fn_map.get_SDTL_function(python_name, positional_args, keyword_args, "horizontal")
                    placeholder = PlaceHolder(function)
                    for arg in node.args:
                        if isinstance(arg, ast.Attribute):
                            if isinstance(arg.value, ast.Name):
                                if arg.value.id in self.dataframe_variables:
                                    placeholder.consume(self.makeDataframeDescription(arg.value.id))
                    return placeholder
                except KeyError:
                    return Unsupported("Unknown function.")

            else:
                return NoTransformOp()

    def makeLoad(self, node):
        filename = os.path.basename(node.args[0].s)

        software_map = { 
            "read_csv" : None,
            "read_excel": "Excel",
            "read_sas": "SAS",
            "read_stata": "Stata"
        }
        try:
            software = software_map[node.func.attr]
        except KeyError:
            return Invalid("Unknown read function.")

        file_format = filename.split(".")[-1]

        load = Load(filename, software=software, file_format=file_format)

        # name_of_file is filename without extension
        name_of_file = filename.split(".")[0]

        variables = self.file_variables[name_of_file].copy()

        load.produce(DataframeDescription(variables=variables))

        # TODO: handle arguments that might take in a MultiIndexed data file

        return load

    def makeMergeDatasets(self, node):
        function = node.func.attr
        merge = MergeDatasets()
        commands = []
        multiple_commands = False

        # whether this merge was made with pd.merge(left, right)
        # as opposed to left.merge(right)
        pd_merge = node.func.value.id == self.pandas_alias

        try:
            if pd_merge:
                left_filename = node.args[0].id
            else:
                left_filename = node.func.value.id
            left_variables = self.dataframe_variables[left_filename].copy()

        except AttributeError:
            left_command = self.visit(node.func.value)
            commands.append(left_command)
            try:
                left_produced = left_command.producesDataframe[0]

                left_filename = left_produced.dataframeName
                left_variables = left_produced.variableInventory.copy()

                multiple_commands = True
            except IndexError:
                return Invalid("Invalid merge argument.")

        try:
            if pd_merge:
                right_filename = node.args[1].id
            else:
                right_filename = node.args[0].id
            right_variables = self.dataframe_variables[right_filename].copy()

        except AttributeError:
            right_command = self.visit(node.args[0])
            commands.append(right_command)
            try:
                right_produced = right_command.producesDataframe[0]

                right_filename = right_produced.dataframeName
                right_variables = right_produced.variableInventory.copy()

                multiple_commands = True
            except IndexError:
                return Invalid("Invalid merge argument.")

        except IndexError:
            return Invalid("Merges must have two dataframes.")

        left_dataframe = self.makeDataframeDescription(left_filename, variables=left_variables)
        right_dataframe = self.makeDataframeDescription(right_filename, variables=right_variables)

        merge.consume(left_dataframe)
        merge.consume(right_dataframe)

        if function == "combine_first":
            left = MergeFileDescription(left_filename, 
                                        merge_type="Sequential", 
                                        update="Master", 
                                        new_row=True)

            right = MergeFileDescription(right_filename, 
                                         merge_type="Sequential", 
                                         update="UpdateMissing", 
                                         new_row=True)

            variables = list(dict.fromkeys(left_variables + right_variables))

        elif function == "merge":
            if node.keywords:
                # map the keywords into a dictionary for easier reference
                keywords = self.getDictFromKeywords(node.keywords)
                by_variables = []   # list of by variable names

                if "indicator" in keywords:
                    if keywords["indicator"]:
                        return Unsupported("Indicator columns are not currently supported by the parsing application.")

                right_names = []

                if "on" in keywords:
                    if isinstance(keywords["on"], ast.Constant):
                        merge.merge_by(VariableSymbolExpression(keywords["on"].value))
                        by_variables.append(keywords["on"].value)

                    elif isinstance(keywords["on"], ast.List):
                        variable_list = [variable.value for variable in keywords["on"].elts]
                        merge.merge_by(VariableListExpression(variable_list))
                        by_variables = by_variables + variable_list

                elif "left_on" in keywords and "right_on" in keywords:
                    if isinstance(keywords["left_on"], ast.Constant):
                        merge.merge_by(VariableSymbolExpression(keywords["left_on"].value))

                    elif isinstance(keywords["left_on"], ast.List):
                        variable_list = [variable.value for variable in keywords["left_on"].elts]
                        merge.merge_by(VariableListExpression(variable_list))

                    if isinstance(keywords["right_on"], ast.Constant):
                        right_names = [VariableSymbolExpression(keywords["right_on"].value)]

                    elif isinstance(keywords["right_on"], ast.List):
                        right_names = [VariableSymbolExpression(variable.value) for variable in keywords["right_on"].elts]

                elif "left_index" in keywords and "right_index" in keywords:
                    pass    # placeholder to prevent this from being unsupported

                else:
                    return Unsupported("Merges without a by variable are not supported.")

                left_renames = None
                right_renames = None

                common_variables = list_intersection(left_variables, right_variables)
                common_variables = list_difference(common_variables, by_variables)
                if common_variables:
                    if "suffixes" in keywords:
                        suffixes = (keywords["suffixes"].elts[0].value, 
                                    keywords["suffixes"].elts[1].value)
                        if suffixes[0] is None:
                            suffixes = ("", suffixes[1])
                        if suffixes[1] is None:
                            suffixes = (suffixes[0], "")
                    else:
                        suffixes = ("_x", "_y")
                    left_renames = [
                        RenamePair(variable, variable + suffixes[0])
                        for variable in common_variables
                    ]
                    right_renames = [
                        RenamePair(variable, variable + suffixes[1])
                        for variable in common_variables
                    ]
                    for variable in common_variables:
                        left_index = left_variables.index(variable)
                        right_index = right_variables.index(variable)
                        left_variables[left_index] = variable + suffixes[0]
                        right_variables[right_index] = variable + suffixes[1]

                # this is the default state; some things might override
                left_update = "Master"
                right_update = "FillNew"
                left_type = right_type = "Sequential"
                left_new_row, right_new_row = (None, None)
                variables = list(dict.fromkeys(left_variables + right_variables))

                # the how keyword determines the join type
                if "how" in keywords:

                    if keywords["how"].value == "inner":
                        left_type = right_type = "OneToOne"
                        left_new_row = right_new_row = False

                    else:
                        left_type = right_type = "Cartesian"

                        if keywords["how"].value == "left":
                            left_new_row = True
                            right_new_row = False

                        elif keywords["how"].value == "right":
                            left_new_row = False
                            right_new_row = True
                            left_update = "FillNew"
                            right_update = "Master"

                        elif keywords["how"].value == "outer":
                            left_new_row = right_new_row = True
                            if merge.mergeByVariables is None:
                                left_renames = None
                                right_renames = None
                
                # inner join is the default
                else:
                    left_type = right_type = "OneToOne"
                    left_new_row = right_new_row = False

                if "left_index" in keywords and "right_index" in keywords:
                    left_type = right_type = "Sequential"
                    merge.mergeByVariables = None

                left = MergeFileDescription(left_filename, 
                                            merge_type=left_type, 
                                            update=left_update, 
                                            new_row=left_new_row, 
                                            renames=left_renames)

                right = MergeFileDescription(right_filename, 
                                             merge_type=right_type, 
                                             update=right_update, 
                                             new_row=right_new_row, 
                                             renames=right_renames, 
                                             merge_by_names=right_names)

            # if there are no keyword arguments, this is a default merge,
            # which we don't support because it produces an empty dataframe
            else:
                return Unsupported("Merges without a by variable are not supported.")

        elif function == "update":
            right_update = "Replace"

            if node.keywords:
                keywords = self.getDictFromKeywords(node.keywords)
                if "overwrite" in keywords:
                    # overwrite is True -> replace
                    # overwrite is False -> update
                    right_update = "Replace" if keywords["overwrite"].value else "UpdateMissing"

            right_drops = []
            for right_variable in right_variables:
                if right_variable not in left_variables:
                    right_drops.append(VariableSymbolExpression(right_variable))

            left = MergeFileDescription(left_filename,
                                        merge_type="Sequential",
                                        update="Master",
                                        new_row=True)

            right = MergeFileDescription(right_filename,
                                         merge_type="Sequential",
                                         update=right_update,
                                         new_row=False,
                                         drops=right_drops)

            variables = left_variables.copy()

        merge.merge_file(left)
        merge.merge_file(right)

        # TODO: add support for merges that work on MultiIndexed dataframes
        merge.produce(self.makeDataframeDescription(None, variables=variables))

        return merge

    def makeRename(self, node_value, source=None, target=None, variable=None):
        rename = Rename()
        rename_dict = {}

        if isinstance(node_value, ast.Call):
            keywords = self.getDictFromKeywords(node_value.keywords)

            if "columns" in keywords:
                rename_dict = ast.literal_eval(self.getSourceString(keywords["columns"]))

            if "inplace" in keywords and keywords["inplace"].value:
                target = source

            if "index" in keywords or ("axis" in keywords and keywords["axis"].value == "index"):
                return Unsupported("Renaming rows is not currently supported by the parsing application.")

        if variable is not None:
            rename_dict[variable.variableName] = node_value.s

        variables = self.dataframe_variables[source].copy()

        if isinstance(node_value, ast.List):
            num_renames = len(node_value.elts)
            if num_renames != len(variables):
                return Invalid("Length of new name list differs from length of original variable list.")
            for x in range(num_renames):
                rename_dict[variables[x]] = node_value.elts[x].s

        # apply the rename to the data
        for old_name, new_name in rename_dict.items():
            try:
                variable_index = variables.index(old_name)
                variables[variable_index] = new_name
                rename.add_pair(old_name, new_name)
            except ValueError:
                return Invalid("Trying to rename a variable that doesn't exist in the data.")

        rename.consume(self.makeDataframeDescription(source))
        if target:
            rename.produce(self.makeDataframeDescription(target, variables=variables))
        else:
            rename.produce(self.makeDataframeDescription(None, variables=variables))

        return rename

    def makeSave(self, node):
        filename = node.args[0].s

        software_map = {
            "to_csv": None,
            "to_excel": "Excel",
            "to_stata": "Stata"
        }
        try:
            software = software_map[node.func.attr]
        except:
            return Invalid("Unknown read function")

        file_format = filename.split(".")[-1]

        save = Save(filename, software=software, file_format=file_format)

        consumed = node.func.value.id

        # name_of_file is filename without extension
        name_of_file = filename.split(".")[0]

        self.file_variables[name_of_file] = self.dataframe_variables[consumed].copy()

        save.consume(self.makeDataframeDescription(consumed))

        return save

    def makeSortCases(self, node):
        sort = SortCases()
        keywords = self.getDictFromKeywords(node.keywords)

        # if a positional argument is present, it's the sort key(s)
        if node.args:
            keys = node.args[0]
        elif "by" in keywords:
            keys = keywords["by"]

        if "axis" in keywords and (keywords["axis"].value == "columns" or keywords["axis"].value == 1):
            return Unsupported("Sorting columns is not currently supported by the parsing application.")

        direction = "ascending"
        if "ascending" in keywords:
            if isinstance(keywords["ascending"], ast.List):
                direction = [
                    "ascending" if elt.value else "descending" 
                    for elt in keywords["ascending"].elts
                ]
            else:
                direction = "descending" if not keywords["ascending"].value else direction

        if isinstance(keys, ast.List):
            if isinstance(direction, list):
                for index, key in enumerate(keys.elts):
                    sort.add_criterion(key.value, direction[index])
            else:
                for key in keys.elts:
                    sort.add_criterion(key.value, direction)

        else:
            sort.add_criterion(keys.value, direction)

        # sorting cases doesn't change anything about the variables
        dataframe = self.makeDataframeDescription(node.func.value.id)

        sort.consume(dataframe)

        if "inplace" in keywords and keywords["inplace"].value:
            sort.produce(dataframe)
        else:
            sort.produce(self.makeDataframeDescription(None, variables=sort.consumesDataframe[0].variableInventory))

        return sort

    def makeSubstring(self, node):
        if isinstance(node.value, ast.Subscript):
            variable = VariableSymbolExpression(node.value.slice.value)

        elif isinstance(node.value, ast.Attribute):
            variable = VariableSymbolExpression(node.value.attr)

        try:
            start = NumericConstantExpression(node.slice.lower.value + 1)

        except AttributeError:
            start = NumericConstantExpression(1)

        try:
            stop = NumericConstantExpression(node.slice.upper.value)

        except AttributeError:
            return Unsupported("SDTL does not support nondeterministic range ends.")

        try:
            step = NumericConstantExpression(node.slice.step.value)

        except AttributeError:
            step = NumericConstantExpression(1)

        arguments = self.makeFunctionArguments([variable, start, stop , step])
        function = FunctionCallExpression(name="substr_by_position", arguments=arguments)

        placeholder = PlaceHolder(function)

        self.consume_and_produce(command=placeholder, dataframe=node.value.value.id)

        return placeholder

    def makeValue(self, value):
        if isinstance(value, str):
            return StringConstantExpression(value)
        elif isinstance(value, int) or isinstance(value, float):
            return NumericConstantExpression(value)
        elif isinstance(value, bool):
            return BooleanConstantExpression(value)
        return MissingValueConstantExpression()

    def makeValueList(self, values):
        return ValueListExpression([self.makeValue(value) for value in values])

    def makeVariableRange(self, node):
        # the only method of specifying a variable range that we support
        # is a call to loc surrounded by a cast to the list type, in which
        # loc has two arguments: a colon by itself (indicating row selection
        # is irrelevant), and the enumerated range, e.g. "var1":"var2"
        return VariableRangeExpression(node.args[0].slice.elts[1].lower.value,
                                       node.args[0].slice.elts[1].upper.value)

    def report(self):
        return self.program

    # (this function isn't done)
    def parseAnyAll(self, node):
        if isinstance(node.func.value, ast.Call):
            command = self.visit(node.func.value)
            # TODO: differentiate between any and all
        else:
            return Invalid("any() and all() must apply to a function.")

        return command

    def parseAppend(self, node):
        top = node.func.value.id
        bottom = node.args[0].id
        return self.makeAppendDatasets([top, bottom])

    # (this function needs revisiting)
    def parseAssign(self, node):
        dataframe = node.func.value.id

        if len(node.keywords) > 1:
            commands = []
            variables = self.dataframe_variables[dataframe].copy()
            for index, keyword in enumerate(node.keywords):
                variable = VariableSymbolExpression(keyword.arg)
                placeholder = self.visit(keyword.value)
                compute = Compute(variable, placeholder.expression)

                if placeholder.consumesDataframe:
                    consumes = placeholder.consumesDataframe
                elif index == 0:
                    consumes = self.makeDataframeDescription(dataframe, variables=variables.copy())
                else:
                    consumes = self.makeDataframeDescription(None, variables=variables.copy())
                    self.df_count = self.df_count + 1

                if keyword.arg not in variables:
                    variables.append(keyword.arg)

                produces = self.makeDataframeDescription(None, variables=variables.copy())

                compute.consume(consumes)
                compute.produce(produces)

                commands.append(compute)

                try:
                    # if the expression was a call to the cut function
                    if placeholder.expression.function in ["cut_list", "cut_range", "cut_freq", "cut_quant"]:
                        subtype = "ordered" if placeholder.keywords["ordered"] else "unordered"
                        set_type = SetDataType(compute.variable, 
                                               "Factor", 
                                               subtype_schema="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Categorical.html",
                                               subtype=subtype)
                        set_type.consume(produces)
                        set_type.produce_consumes()

                        commands.append(set_type)

                except AttributeError:
                    pass

                # if labels were assigned, separate out the SetValueLabels
                if placeholder.command:
                    try:
                        placeholder.command.variables.append(compute.variable)
                        placeholder.command.consume(produces)
                        placeholder.command.produce_consumes()
                    except AttributeError:
                        pass

                    commands.append(placeholder.command)

            return commands

        else:
            variables = self.dataframe_variables[dataframe].copy()
            keyword = node.keywords[0]
            variable = VariableSymbolExpression(keyword.arg)
            expression = self.visit(keyword.value)
            if isinstance(expression, PlaceHolder):
                compute = Compute(variable, expression.expression)
            else:
                compute = Compute(variable, expression)
            if keyword.arg not in variables:
                variables.append(keyword.arg)
            compute.consume(self.makeDataframeDescription(dataframe))
            compute.produce(self.makeDataframeDescription(None, variables=variables.copy()))

            # if the expression was a call to the cut function
            if isinstance(expression, PlaceHolder) and expression.command:
                subtype = "ordered" if expression.keywords["ordered"] else "unordered"
                set_type = SetDataType([compute.variable], 
                                       "Factor", 
                                       subtype_schema="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Categorical.html",
                                       subtype=subtype)
                set_type.consume(compute.producesDataframe[0])
                set_type.produce_consumes()

                # if labels were assigned, separate out the SetValueLabels
                if expression.command:
                    try:
                        expression.command.variables.append(compute.variable)
                        expression.command.consume(compute.producesDataframe[0])
                        expression.command.produce_consumes()
                    except AttributeError:
                        pass
                    return [compute, set_type, expression.command]

                return [compute, set_type]

            return compute

    def parseCut(self, node):
        keywords = self.getDictFromKeywords(node.keywords)

        group_placeholder = self.visit(node.args[0])

        # the variable being used to assign rows to groups
        group_var = group_placeholder.expression

        # whether intervals are closed on the right side
        right_bool = ("right" not in keywords) or (keywords["right"].value)
        right = StringConstantExpression("Right" if right_bool else "Left")

        # whether the lowest group is inclusive on the left
        left_bool = "include_lowest" in keywords and keywords["include_lowest"].value
        exp4 = BooleanConstantExpression(bool(right_bool and left_bool))

        # list of breakpoints/quantiles or scalar number of groups
        bins_raw = self.visit(node.args[1])

        # whether the labels are ordered
        ordered = bool(("ordered" not in keywords) or keywords["ordered"].value)

        if node.func.attr == "cut":

            # SDTL cut_range: divide rows into k groups of equal width
            if isinstance(bins_raw, NumericConstantExpression):
                arguments = self.makeFunctionArguments([group_var, bins_raw, right, exp4])
                function = FunctionCallExpression(name="cut_range", arguments=arguments)

            # SDTl cut_list: use the list of breakpoints to divide cases into groups
            else:
                bins = self.makeValueList(bins_raw)
                exp5 = StringConstantExpression("Int_code")

                arguments = self.makeFunctionArguments([group_var, bins, right, exp4, exp5])
                function = FunctionCallExpression(name="cut_list", arguments=arguments)

        # we can't parse labels in either qcut variant because they depend on the data
        elif node.func.attr == "qcut":

            # SDTL cut_freq: divide rows into k groups with equal number of rows in each
            if isinstance(bins_raw, NumericConstantExpression):
                arguments = self.makeFunctionArguments([group_var, bins_raw, right, exp4])
                function = FunctionCallExpression(name="cut_freq", arguments=arguments)

            # SDTL cut_quant: divide rows into groups based on the list of quantile boundaries
            else:
                bins = self.makeValueList(bins_raw)
                arguments = self.makeFunctionArguments([group_var, bins, right])
                function = FunctionCallExpression(name="cut_quant", arguments=arguments)

        # add a SetValueLabels command if the labels can be determined
        if "labels" in keywords:
            # if the labels are defined explicitly, use those
            labels_list = self.visit(keywords["labels"])
            if labels_list:
                value_labels = [
                    ValueLabel(str(x), label)
                    for x, label in enumerate(labels_list)
                ]
            # otherwise, we can still determine labels if they're the default integer bins
            else:
                # the number of bins determines the number of labels
                if isinstance(node.args[1].value, int):
                    value_labels = [
                        ValueLabel(str(x), str(x))
                        for x in range(node.args[1].value)
                    ]
                # if a list of breakpoints or quantiles was given, there are (length - 1) bins
                else:
                    value_labels = [
                        ValueLabel(str(x), str(x))
                        for x in range(len(node.args[1]) - 1)
                    ]
            # this will be incomplete until the surrounding assignment is parsed
            set_labels = SetValueLabels(variables=[], labels=value_labels)

            placeholder = PlaceHolder(function, command=set_labels, ordered=ordered)

        else:
            message = Message("Since the boundaries of the groups depend on the data, a SetValueLabels is not possible.",
                              severity="Warning")
            placeholder = PlaceHolder(function, command=message, ordered=ordered)

        placeholder.consume(group_placeholder.consumesDataframe[0])

        return placeholder

    def parseConcat(self, node):
        if node.func.value.id == self.pandas_alias:
            keywords = self.getDictFromKeywords(node.keywords)

            # if this is a horizontal concatenation, it's Unsupported
            if "axis" in keywords and (keywords["axis"] == 1 or keywords["axis"] == "columns"):
                return Unsupported("Horizontal concatenation is not supported.")

            else:
                inner = "join" in keywords and keywords["join"].value == "inner"
                dataframes = [elt.id for elt in node.args[0].elts]
                return self.makeAppendDatasets(dataframes, inner=inner)
        else:
            return Unsupported("Unrecognized concat() function.")

    def parseDrop(self, node):
        source_df = node.func.value.id
        if source_df in self.dataframe_variables:
            return self.makeDrop(node, source=source_df)
        else:
            return Invalid("Trying to drop variables from a dataframe that doesn't exist.")

    def parseDropna(self, node):
        keywords = self.getDictFromKeywords(node.keywords)

        condition = FunctionCallExpression(name="missing")

        if keywords["how"].value == "all":
            condition.add_argument(AllVariablesExpression())

        drop_cases = DropCases(condition)

        self.consume_and_produce(command=drop_cases, dataframe=node.func.value.id)

        return drop_cases

    def parseIsin(self, node):
        all_expr = AllVariablesExpression()
        in_list = self.visit(node.args[0])
        in_expr = self.makeValueList(in_list)

        arguments = self.makeFunctionArguments([all_expr, in_expr])
        function = FunctionCallExpression(name="lgcl_any", arguments=arguments)

        placeholder = PlaceHolder(function)

        self.consume_and_produce(command=placeholder, dataframe=node.func.value.id)

        return placeholder

    def parseIsna(self, node):
        variables = self.dataframe_variables[node.func.value.id].copy()
        arguments = self.makeFunctionArguments([VariableListExpression(variables)])
        function = FunctionCallExpression(name="missing_list", arguments=arguments)

        placeholder = PlaceHolder(function)

        self.consume_and_produce(command=placeholder, dataframe=node.func.value.id)

        return placeholder

    def parseLoc(self, node):
        if isinstance(node.slice, ast.Tuple):

            compare = node.slice.elts[0]
            variable = VariableSymbolExpression(node.slice.elts[1].value)

            placeholder = self.visit(compare)
            ifrows = self.copy_placeholder(placeholder, command_type=IfRows)

            compute = Compute(variable=variable)
            ifrows.add_then(compute)

            return ifrows

        elif isinstance(node.slice, ast.Compare):

            placeholder = self.visit(node.slice)

            return self.copy_placeholder(placeholder, command_type=KeepCases)

        return Unsupported("Using loc to specify new rows is not supported.")

    def parseRename(self, node):
        source_df = node.func.value.id
        if source_df in self.dataframe_variables:
            return self.makeRename(node, source=source_df)

        else:
            return Invalid("Trying to perform a rename on a dataframe that doesn't exist.")

    def printDataframeVariables(self):
        print("{")
        for dataframe, variables in self.dataframe_variables.items():
            print("    " + dataframe + ": " + str(variables))
        print("}")

    def produceDataframe(self, dataframe):
        self.dataframe_variables[dataframe.dataframeName] = dataframe.variableInventory.copy()
        if dataframe.rowDimensions:
            self.row_dimensions[dataframe.dataframeName] = dataframe.rowDimensions.copy()
        if dataframe.columnDimensions:
            self.column_dimensions[dataframe.dataframeName] = dataframe.columnDimensions.copy()

    def produceDataframes(self, dataframes):
        if isinstance(dataframes, list):
            for dataframe in dataframes:
                self.produceDataframe(dataframe)
        else:
            self.produceDataframe(dataframes)

    # ---------------------------VISITOR FUNCTIONS-----------------------------

    def visit_Add(self, node):
        return "addition"

    def visit_Assign(self, node):
        target = node.targets[0]    # TODO: handle multiple targets
        value = None
        #print(node.lineno)

        # copy the current state of globals in case there are errors to recover from
        file_variables_before = self.file_variables.copy()
        dataframe_variables_before = self.dataframe_variables.copy()

        # traditional crashing behavior doesn't look good from the users' perspective,
        # so we try to catch any failure and tell them where to simplify their script
        # (if you are debugging a new feature, you will have a much easier time if you
        # temporarily remove this surrounding try-except to get the traceback you need)
        try:
            if isinstance(target, ast.Subscript):
                index = self.visit(target)

                if isinstance(index, IfRows):
                    # TODO: if project scope is expanded to include multi-dataframe computes,
                    # use the consumed and produced dataframes accordingly in the output
                    placeholder = self.visit(node.value)
                    try:
                        index.thenCommands[0].expression = placeholder.expression
                    except AttributeError:
                        index.thenCommands[0].expression = placeholder
                    value = index

                elif index.producesDataframe:
                    source = index.producesDataframe[0].dataframeName
                    # if the target of the assignment is the columns attribute, this is a Rename
                    if hasattr(target.value, "attr") and target.value.attr == "columns":
                        value = self.makeRename(node.value, source=source, target=source, variable=index.expression)

                    # if the target of the assignment is a dataframe column, this is a Compute
                    else:
                        variables = self.dataframe_variables[source].copy()
                        placeholder = self.visit(node.value)
                        if isinstance(placeholder, Unsupported):
                            value = placeholder
                        else:
                            if isinstance(placeholder, ExpressionBase):
                                value = Compute(index.expression, placeholder)
                                for dataframe in index.consumesDataframe:
                                    value.consume(dataframe)
                            else:
                                value = Compute(index.expression, placeholder.expression)
                                for dataframe in placeholder.consumesDataframe:
                                    value.consume(dataframe)

                            try:
                                produces = placeholder.producesDataframe[0]
                                produces.dataframeName = source
                            except (AttributeError, IndexError):
                                produces = index.producesDataframe[0]

                            if index.expression.variableName not in variables:
                                variables.append(index.expression.variableName)
                            produces.variableInventory = variables

                            value.produce(produces)

                            try:
                                # if the expression was a call to the cut function
                                if placeholder.expression.function in ["cut_list", "cut_range", "cut_freq", "cut_quant"]:
                                    commands = [value]

                                    subtype = "ordered" if placeholder.keywords["ordered"] else "unordered"

                                    set_type = SetDataType([value.variable], 
                                                           "Factor", 
                                                           subtype_schema="https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Categorical.html",
                                                           subtype=subtype)
                                    set_type.consume(produces)
                                    set_type.produce_consumes()

                                    commands.append(set_type)

                                    # if value labels were set, separate out the SetValueLabels
                                    if placeholder.command:
                                        if isinstance(placeholder.command, SetValueLabels):
                                            placeholder.command.variables.append(value.variable)
                                            placeholder.command.consume(produces)
                                            placeholder.command.produce_consumes()

                                        commands.append(placeholder.command)

                                    value = commands
                            except AttributeError:
                                pass

            elif isinstance(target, ast.Name):
                value = self.visit(node.value)
                if hasattr(value, "producesDataframe"):
                    value.producesDataframe[0] = copy.copy(value.producesDataframe[0])
                    value.producesDataframe[0].dataframeName = target.id
                    if isinstance(value, Collapse):
                        value.outputDatasetName = target.id
                elif isinstance(value, list):
                    if isinstance(value[-2], SetDataType):
                        last_variables = value[-3].producesDataframe[0].variableInventory.copy()
                        value[-3].producesDataframe[0] = copy.copy(value[-3].producesDataframe[0])
                        value[-3].producesDataframe[0].dataframeName = target.id
                        for command in value:
                            if isinstance(command, SetValueLabels) or isinstance(command, SetDataType):
                                self.copy_dataframes(value[-3], command)

                    else:
                        value[-1].producesDataframe[0] = copy.copy(value[-1].producesDataframe[0])
                        value[-1].producesDataframe[0].dataframeName = target.id

            elif isinstance(target, ast.Attribute) and target.attr == "columns":

                if target.value.id in self.dataframe_variables:
                    value = self.makeRename(node.value, source=target.value.id, target=target.value.id)

                else:
                    value = Invalid("Trying to perform a rename on a dataframe that doesn't exist.")
            else:
                pass

            if isinstance(value, list):
                for index, command in enumerate(value):
                    command.sourceInformation = self.getSourceInfo(node)
                    if not isinstance(command, InformBase):
                        self.produceDataframes(command.producesDataframe)
                    self.program.add_command(command)
                #self.clearActiveDataframes()
            else:
                value.sourceInformation = self.getSourceInfo(node)

                # if this is not an informational command, then it must create a new dataframe
                if not isinstance(value, InformBase):
                    self.produceDataframes(value.producesDataframe)

                self.program.add_command(value)
                #print(get_json(self.program, indent=4))
            #self.printDataframeVariables()
            self.clearActiveDataframes()

        except:
            error_text = (
                f"Error: the syntax used in lines {node.lineno} through {node.end_lineno} "
                f"of your script was not recognized by the Python parser. Most likely, this "
                f"is not an error on your behalf. The C2Metadata project realized early on "
                f"that it could never have complete coverage of any source language, so we "
                f"decided to aim for only the most commonly used operations. Due to the very "
                f"nature of Python syntax, there are often many different ways to accomplish "
                f"the same task, so you could try to simplify the expression by breaking it "
                f"down into its composite parts, or rewrite it entirely using different "
                f"logic, then see if the new version parses correctly. As some operations "
                f"are explicitly out of scope, this won't always work (although every effort "
                f"has been made to catch anything known to be out of scope and give a more "
                f"informative message than this generic catch-all). If you believe that the "
                f"scope of this parser should be expanded to handle your use case, or that "
                f"this message was triggered due to a legitimate bug in the parser, please "
                f"submit a new issue to the project repository at "
                f"https://gitlab.com/c2metadata/python-to-sdtl and the project maintainers "
                f"will look into it. This project is open source, so if you feel so "
                f"inclined, we encourage you to contribute your own bugfix/implementation."
            )
            message = Message(error_text, severity="Error")
            message.sourceInformation = self.getSourceInfo(node)
            self.program.add_command(message)

            self.clearActiveDataframes()

            self.file_variables = file_variables_before.copy()
            self.dataframe_variables = dataframe_variables_before.copy()

    def visit_Attribute(self, node):
        if isinstance(node.value, ast.Name):
            placeholder = PlaceHolder(VariableSymbolExpression(node.attr))
            placeholder.consume(self.makeDataframeDescription(node.value.id))
            placeholder.produce(self.makeDataframeDescription(node.value.id, variables=[node.attr]))
            return placeholder

    def visit_BinOp(self, node):
        try:
            left = self.visit(node.left)
            left_consumes = left.consumesDataframe
            left_produces = left.producesDataframe
        except (ValueError, TypeError, AttributeError) as outer:
            left_produces = None
            try:
                left_consumes = left.consumesDataframe
            except (ValueError, TypeError, AttributeError) as inner:
                left_consumes = None
                left = PlaceHolder(left)

        try:
            right = self.visit(node.right)
            right_consumes = right.consumesDataframe
            right_produces = right.producesDataframe
        except (ValueError, TypeError, AttributeError) as outer:
            right_produces = None
            try:
                right_consumes = right.consumesDataframe
            except (ValueError, TypeError, AttributeError) as inner:
                right_consumes = None
                right = PlaceHolder(right)

        name = self.visit(node.op)
        arguments = self.makeFunctionArguments([left.expression, right.expression])

        function = FunctionCallExpression(name=name, arguments=arguments)
        placeholder = PlaceHolder(function)

        placeholder.consume(self.combine_consumes(left_consumes, right_consumes))
        placeholder.produce(self.combine_produces(left_produces, right_produces))

        return placeholder

    # The python equivalent of KeepCases uses bitwise logical operators
    def visit_BitAnd(self, node):
        return "and"

    def visit_BitOr(self, node):
        return "or"

    def visit_Call(self, node):
        if isinstance(node.func, ast.Attribute):
            fn_map = {
                "all":          self.parseAnyAll,
                "any":          self.parseAnyAll,
                "assign":       self.parseAssign,
                "append":       self.parseAppend,
                "cut":          self.parseCut,
                "combine_first": self.makeMergeDatasets,
                "concat":       self.parseConcat,
                "drop":         self.parseDrop,
                "dropna":       self.parseDropna,
                "isin":         self.parseIsin,
                "isna":         self.parseIsna,
                "merge":        self.makeMergeDatasets,
                "qcut":         self.parseCut,
                "read_csv":     self.makeLoad,
                "read_excel":   self.makeLoad,
                "read_sas":     self.makeLoad,
                "read_stata":   self.makeLoad,
                "rename":       self.parseRename,
                "sort_values":  self.makeSortCases,
                "to_csv":       self.makeSave,
                "to_excel":     self.makeSave,
                "to_stata":     self.makeSave,
                "update":       self.makeMergeDatasets,
            }
            try:
                return fn_map[node.func.attr](node)
            except KeyError:
                return self.makeLibraryFunction(node)

        else:
            return NoTransformOp()

    def visit_Compare(self, node):
        try:
            left = self.visit(node.left)
            left_consumes = left.consumesDataframe
            left_produces = left.producesDataframe
        except AttributeError as outer:
            left_produces = None
            try:
                left_consumes = left.consumesDataframe
            except AttributeError as inner:
                left_consumes = None

        if len(node.comparators) == 1:
            try:
                right = self.visit(node.comparators[0])
                if isinstance(right, PlaceHolder):
                    right_expression = right.expression
                else:
                    right_expression = right
                right_consumes = right.consumesDataframe
                right_produces = right.producesDataframe
            except AttributeError as outer:
                right_produces = None
                try:
                    right_consumes = right.consumesDataframe
                except AttributeError as inner:
                    right_consumes = None
        else:
            # TODO
            pass

        name = self.visit(node.ops[0])  # TODO: more than one op?
        arguments = self.makeFunctionArguments([left.expression, right_expression])

        function = FunctionCallExpression(name=name, arguments=arguments)
        placeholder = PlaceHolder(function)

        placeholder.consume(self.combine_consumes(left_consumes, right_consumes))
        placeholder.produce(self.combine_produces(left_produces, right_produces))

        return placeholder

    def visit_Constant(self, node):
        if isinstance(node.value, bool):
            return node.value
        return self.makeValue(node.value)

    def visit_Div(self, node):
        return "division"

    def visit_Eq(self, node):
        return "eq"

    def visit_Expr(self, node):
        if isinstance(node.value, ast.Call):
            value = self.visit(node.value)
        else:
            # we need this to avoid parsing docstrings
            return

        # if this expression represents an SDTL command, add it to the command list
        if hasattr(value, "command"):
            if hasattr(value, "producesDataframe"):
                self.produceDataframes(value.producesDataframe)
            value.sourceInformation = self.getSourceInfo(node)
            self.program.add_command(value)
        elif isinstance(value, InformBase):
            value.sourceInformation = self.getSourceInfo(node)
            self.program.add_command(value)

        self.clearActiveDataframes()

    def visit_For(self, node):
        # we're assuming here that a for loop will never appear within an assign or call
        # TODO: implement proper parsing of for loops into SDTL LoopOverList
        value = Unsupported("The parsing application does not currently support for loops.")
        value.sourceInformation = self.getSourceInfo(node)
        self.program.add_command(value)

    def visit_Gt(self, node):
        return "gt"

    def visit_GtE(self, node):
        return "ge"

    def visit_Import(self, node):
        no_transform_op = NoTransformOp()

        # record the user-specified aliases for pandas and numpy
        for name in node.names:
            if name.asname is not None:
                if name.name == "pandas":
                    self.pandas_alias = name.asname
                if name.name == "numpy":
                    self.numpy_alias = name.asname

        # get the source information for this import statement
        no_transform_op.sourceInformation = self.getSourceInfo(node)

        self.program.add_command(no_transform_op)

    def visit_Invert(self, node):
        return "not"

    def visit_Lambda(self, node):
        args = [arg.arg for arg in node.args.args]
        for arg in args:
            self.lambda_variables[arg] = []
        body = self.visit(node.body)
        dataframe = body.producesDataframe[0]
        for arg in args:
            self.lambda_variables[arg] = dataframe.variableInventory.copy()
        if body.consumesDataframe[0].dataframeName in args:
            body.consumesDataframe.pop()
        return body

    def visit_List(self, node):
        return [(-(elt.operand.value) if isinstance(elt, ast.UnaryOp) else elt.value) for elt in node.elts]

    def visit_Lt(self, node):
        return "lt"

    def visit_LtE(self, node):
        return "le"

    def visit_Mult(self, node):
        return "multiplication"

    def visit_NotEq(self, node):
        return "ne"

    def visit_Sub(self, node):
        return "subtraction"

    def visit_Subscript(self, node):
        index, dataframe = (None, None)
        if isinstance(node.value, ast.Attribute) and node.value.attr == "loc":
            return self.parseLoc(node)

        if isinstance(node.slice, ast.Index):
            if isinstance(node.slice.value, ast.Str):
                index = VariableSymbolExpression(node.slice.value.s)
            elif isinstance(node.slice.value, ast.Num):
                index = NumberRangeExpression(node.slice.value.n)
            elif isinstance(node.slice.value, ast.Constant):
                if isinstance(node.slice.value.value, str):
                    index = VariableSymbolExpression(node.slice.value.value)
                else:
                    index = NumberRangeExpression(node.slice.value.value)
            else:
                return self.makeCases(node)
        elif isinstance(node.slice, ast.List):
            return self.makeKeep(node)
        elif isinstance(node.slice, ast.Slice):
            return self.makeSubstring(node)
        elif isinstance(node.slice, ast.Constant):
            index = VariableSymbolExpression(node.slice.value)
        else:
            return self.makeCases(node)

        if isinstance(node.value, ast.Name):
            dataframe = self.makeDataframeDescription(node.value.id)
        elif isinstance(node.value, ast.Attribute):
            dataframe = self.makeDataframeDescription(node.value.value.id)

        placeholder = PlaceHolder(index)
        placeholder.consume(dataframe)
        placeholder.produce_consumes()

        return placeholder

    def visit_UnaryOp(self, node):
        name = self.visit(node.op)

        placeholder = self.visit(node.operand)

        arguments = self.makeFunctionArguments([placeholder.expression])

        function = FunctionCallExpression(name=name, arguments=arguments)

        placeholder.expression = function

        return placeholder
